﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace NFox.Cad.ExtendMethods
{
    public static class CollectionEx
    {

        public static ObjectIdCollection ToCollection(this IEnumerable<ObjectId> ids)
        {
            ObjectIdCollection idCol = new ObjectIdCollection();
            foreach (ObjectId id in ids)
                idCol.Add(id);
            return idCol;
        }

        public static DBObjectCollection ToCollection<T>(this IEnumerable<T> objs) where T : DBObject
        {
            DBObjectCollection objCol = new DBObjectCollection();
            foreach (T obj in objs)
                objCol.Add(obj);
            return objCol;
        }

        public static DoubleCollection ToCollection(this IEnumerable<double> doubles)
        {
            DoubleCollection doubleCol = new DoubleCollection();
            foreach (double d in doubles)
                doubleCol.Add(d);
            return doubleCol;
        }

        public static Point2dCollection ToCollection(this IEnumerable<Point2d> pts)
        {
            Point2dCollection ptCol = new Point2dCollection();
            foreach (Point2d pt in pts)
                ptCol.Add(pt);
            return ptCol;
        }

        public static Point3dCollection ToCollection(this IEnumerable<Point3d> pts)
        {
            Point3dCollection ptCol = new Point3dCollection();
            foreach (Point3d pt in pts)
                ptCol.Add(pt);
            return ptCol;
        }

        public static List<ObjectId> GetObjectIds(this ObjectIdCollection ids)
        {
            return ids.Cast<ObjectId>().ToList();
        }

    }
}

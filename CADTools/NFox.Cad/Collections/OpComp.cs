﻿using Autodesk.AutoCAD.DatabaseServices;
using System.Collections.Generic;

namespace NFox.Cad.Collections
{
    public class OpComp : OpEqual
    {

        public string Content { get; }

        public override string Name
        {
            get { return "Comp"; }
        }

        public OpComp(string content, TypedValue value)
            : base(value)
        {
            Content = content;
        }

        public OpComp(string content, int code)
            : base(code)
        {
            Content = content;
        }

        public OpComp(string content, int code, object value)
            : base(code, value)
        {
            Content = content;
        }

        public OpComp(string content, DxfCode code, object value)
            : base(code, value)
        {
            Content = content;
        }

        public override IEnumerable<TypedValue> GetValues()
        {
            yield return new TypedValue(-4, Content);
            yield return Value;
        }

    }
}

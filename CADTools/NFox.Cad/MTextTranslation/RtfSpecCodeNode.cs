﻿
namespace NFox.Cad.MTextTranslation
{

    public enum RtfSpecCodeNodeType
    {
        Page,             //P
        Space,            //~
        Brace,            //{
        BraceCounter,     //}
    }

    public class RtfSpecCodeNode : RtfNode
    {

        private readonly static string[] _typeCodes =
            new string[] { "P", "~", "{", "}" };

        private readonly static string[] _specCodes =
            new string[] { "\n", " ", "{", "}" };

        public RtfSpecCodeNode(RtfSpecCodeNodeType nodeType)
        {
            _nodeClassType = RtfNodeClassType.SpecCode;
            _key = (int)nodeType;
        }

        public override string Contents => $"\\{_typeCodes[_key]}";

    }
}

﻿using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace NFox.Cad.ExtendMethods
{
    /// <summary>
    /// Convert GeCurve2d To DbCurve
    /// </summary>
    public static class Curve2dEx
    {

        #region Curve2d

        /// <summary>
        /// 按矩阵转换Ge2d曲线为Db曲线
        /// </summary>
        /// <param name="curve">Ge2d曲线</param>
        /// <param name="mat">曲线转换矩阵</param>
        /// <returns>Db曲线</returns>
        public static Curve ToCurve(this Curve2d curve, Matrix3d mat)
        {

            if (curve is LineSegment2d)
            {
                return ToCurve((LineSegment2d)curve, mat);
            }
            else if (curve is NurbCurve2d)
            {
                return ToCurve((NurbCurve2d)curve, mat);
            }
            else if (curve is EllipticalArc2d)
            {
                return ToCurve((EllipticalArc2d)curve, mat);
            }
            else if (curve is CircularArc2d)
            {
                return ToCurve((CircularArc2d)curve, mat);
            }
            else
            {
                //待续
                return null;
            }
        }

        #endregion

        #region CircularArc2d
        
        /// <summary>
        /// 判断点是否属于圆,On or Inside
        /// </summary>
        /// <param name="ca2d"></param>
        /// <param name="pnt"></param>
        /// <returns></returns>
        public static bool IsIn(this CircularArc2d ca2d, Point2d pnt)
        {
            return ca2d.IsOn(pnt) || ca2d.IsInside(pnt);
        }

        /// <summary>
        /// 按矩阵转换GeCircularArc2d为DbCircle or DbArc
        /// </summary>
        /// <param name="ca2d">GeCircularArc2d</param>
        /// <param name="mat">曲线转换矩阵</param>
        /// <returns>DbCircle or DbArc</returns>
        public static Curve ToCurve(this CircularArc2d ca2d, Matrix3d mat)
        {
            Curve c = ToCurve(ca2d);
            c.TransformBy(mat);
            return c;
        }

        /// <summary>
        /// 转换GeCircularArc2d为DbCircle or DbArc
        /// </summary>
        /// <param name="ca2d">GeCircularArc2d</param>
        /// <returns>DbCircle or DbArc</returns>
        public static Curve ToCurve(this CircularArc2d ca2d)
        {
            if (ca2d.IsClosed())
            {
                return ToCircle(ca2d);
            }
            else
            {
                return ToArc(ca2d);
            }
        }

        /// <summary>
        /// 转换GeCircularArc2d为DbCircle
        /// </summary>
        /// <param name="c2d">GeCircularArc2d</param>
        /// <returns>DbCircle</returns>
        public static Circle ToCircle(this CircularArc2d c2d)
        {
            return
                new Circle(
                    new Point3d(new Plane(), c2d.Center),
                    new Vector3d(0, 0, 1),
                    c2d.Radius);
        }

        /// <summary>
        /// 转换GeCircularArc2d为DbArc
        /// </summary>
        /// <param name="a2d">GeCircularArc2d</param>
        /// <returns>DbArc</returns>
        public static Arc ToArc(this CircularArc2d a2d)
        {
            double startangle, endangle;
            double refangle = a2d.ReferenceVector.Angle;

            if (a2d.IsClockWise)
            {
                startangle = -a2d.EndAngle - refangle;
                endangle = -a2d.StartAngle - refangle;
            }
            else
            {
                startangle = a2d.StartAngle + refangle;
                endangle = a2d.EndAngle + refangle;
            }

            return
                new Arc(
                    new Point3d(new Plane(), a2d.Center),
                    Vector3d.ZAxis,
                    a2d.Radius,
                    startangle,
                    endangle);

        }

        #endregion

        #region EllipticalArc2d

        //椭圆弧
        public static Ellipse ToCurve(this EllipticalArc2d ea2d, Matrix3d mat)
        {
            Ellipse e = ToCurve(ea2d);
            e.TransformBy(mat);
            return e;
        }

        public static Ellipse ToCurve(this EllipticalArc2d ea2d)
        {
            Plane plane = new Plane();
            Ellipse ell =
                new Ellipse(
                    new Point3d(plane, ea2d.Center),
                    new Vector3d(0, 0, 1),
                    new Vector3d(plane, ea2d.MajorAxis) * ea2d.MajorRadius,
                    ea2d.MinorRadius / ea2d.MajorRadius,
                    0,
                    Math.PI * 2);
            if (!ea2d.IsClosed())
            {
                if (ea2d.IsClockWise)
                {
                    ell.StartAngle = -ell.GetAngleAtParameter(ea2d.EndAngle);
                    ell.EndAngle = -ell.GetAngleAtParameter(ea2d.StartAngle);
                }
                else
                {
                    ell.StartAngle = ell.GetAngleAtParameter(ea2d.StartAngle);
                    ell.EndAngle = ell.GetAngleAtParameter(ea2d.EndAngle);
                }
            }
            return ell;
        }

        #endregion

        #region Line2d

        public static Xline ToCurve(this Line2d line2d)
        {
            Plane plane = new Plane();
            return
                new Xline
                {
                    BasePoint = new Point3d(plane, line2d.PointOnLine),
                    SecondPoint = new Point3d(plane, line2d.PointOnLine + line2d.Direction)
                };
        }

        public static Xline ToCurve(this Line2d line2d, Matrix3d mat)
        {
            Xline xl = ToCurve(line2d);
            xl.TransformBy(mat);
            return xl;

        }

        public static LineSegment2d ToLineSegment2d(this Line2d line2d, double fromParameter, double toParameter)
        {
            return
                new LineSegment2d
                (
                    line2d.EvaluatePoint(fromParameter),
                    line2d.EvaluatePoint(toParameter)
                );
        }

        #endregion

        #region LineSegment2d

        public static Line ToCurve(this LineSegment2d ls2d, Matrix3d mat)
        {
            Line l = ToCurve(ls2d);
            l.TransformBy(mat);
            return l;
        }

        public static Line ToCurve(this LineSegment2d ls2d)
        {
            Plane plane = new Plane();
            return
                new Line(
                    new Point3d(plane, ls2d.StartPoint),
                    new Point3d(plane, ls2d.EndPoint));
        }

        #endregion

        #region NurbCurve2d

        public static Spline ToCurve(this NurbCurve2d nc2d, Matrix3d mat)
        {
            Spline spl = ToCurve(nc2d);
            spl.TransformBy(mat);
            return spl;
        }

        public static Spline ToCurve(this NurbCurve2d nc2d)
        {
            int i;
            Plane plane = new Plane();
            Point3dCollection ctlpnts = new Point3dCollection();
            for (i = 0; i < nc2d.NumControlPoints; i++)
            {
                ctlpnts.Add(new Point3d(plane, nc2d.GetControlPointAt(i)));
            }

            DoubleCollection knots = new DoubleCollection();
            foreach (double knot in nc2d.Knots)
            {
                knots.Add(knot);
            }

            DoubleCollection weights = new DoubleCollection();
            for (i = 0; i < nc2d.NumWeights; i++)
            {
                weights.Add(nc2d.GetWeightAt(i));
            }

            NurbCurve2dData ncdata = nc2d.DefinitionData;

            return
                new Spline(
                    ncdata.Degree,
                    ncdata.Rational,
                    nc2d.IsClosed(),
                    ncdata.Periodic,
                    ctlpnts,
                    knots,
                    weights,
                    0,
                    nc2d.Knots.Tolerance);
        }

        #endregion


    }
}

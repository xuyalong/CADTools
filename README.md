# CADTools

#### 介绍
AutoCad 扩展工具平台

#### 软件架构

最终程序的组织结构:

- 插件目录
    - main.dll   ----cad需要加载的主dll
    - support    ----内裤目录
        - xxx.dll    ----内裤的dll文件
    - plugins    ----插件的目录
        - plugin1.dll  ----用户自己编写的插件，由main.dll的程序进行加载到cad
        - plugin2.dll  ----同上
        - ...

#### 安装教程

1. 下载 `CadTools.dll`,`NFox.Cad.dll`,`NFox.Basal.dll`三个dll到一个目录
2. 在第一步的目录里建立 `Plugins` 目录
3. 在cad里运行netload命令加载`CadTools.dll`
4. 将您编写的插件dll放在第二步建立的`Plugins`目录里
5. 重启cad之后会默认加载所有的插件

#### 插件编写说明

1. 新建一个类库项目
2. 添加下列引用：
```csharp
accoremgd.dll
acdbmgd.dll
acmgd.dll
adwindows.dll
cadtools.dll
nfox.basal.dll
nfox.cad.dll
presentationcore,
presentationframework
system.componentmodel.composition
```

3. 添加using：
```csharp
using System.ComponentModel.Composition;
using Autodesk.Windows;
using CADTools;
using Autodesk.AutoCAD.Runtime;
```


4. 在类上添加特性：`[Export(typeof(IPlugin))]`
5. 类继承`PluginBase`抽象类
6. 实现`MenuTab`属性，具体语法见menutab语法说明
7. 常规cad插件编写

#### MenuTab语法说明

MenuTab 其实就是一个RibbonTab的对象，可以按照常规的写法，一步步的生成这个对象，为了简化操作特自定义了一系列的函数来简化构造RibbonTab对象。

1. 定义一个Ribbon选项卡对象

语法： `new Tab("xxx"){}`

2. 在选项卡对象大括号内添加Ribbon面板对象

语法：`new Panel("测试"){}`,需要多少个面板就添加多少个
	
现在代码类似这个样子：

```csharp
new Tab("插件平台")
{
    new Panel("测试1"){},
    new Panel("测试2"){},
    new Panel("测试3"){},
    ...
}
```

3. 在面板对象大括号内添加按钮 

- 普通按钮语法：

    `{name, cmd, [image], [toolTip], [size], [orient]}` 

    解释：

    `{功能名称、功能命令、功能图标[默认缺省]、鼠标悬停提示[默认缺省]、图标大小[默认缺省]、文字和图标的位置[默认缺省]}`
    - `name`string类型
    - `cmd`string类型
    - `image`Bitmap类型
    - `toolTip`RibbonToolTip类型
    - `size`RibbonItemSize类型
    - `orient`Orientation枚举类型

- 换行语法：

    `{BreakType.xxx}` 

    解释：

    `BreakType`为一个枚举类型，分别为：
    - `BreakType.Row` 按钮换行
    - `BreakType.Panel` 面板换行，面板换行的效果就是面板下拉显示的那个区域

- 分隔符语法：

    `{RibbonSeparatorStyle.xxx}`

    解释：

    `RibbonSeparatorStyle`为一个枚举类型，分别为：
    - `RibbonSeparatorStyle.Line` 直线
    - `RibbonSeparatorStyle.Spacer` 空白
    - `RibbonSeparatorStyle.Invisible` 我也不知道是啥

4. 面板内除了添加上述三种按钮之外，还可以添加下拉按钮和行面板

- 下拉按钮语法

    `new SplitButton("heihei"){}` 

    特殊性提示：

    - 下拉按钮，只能添加普通按钮
    - 下拉按钮会自动排列，不需要换行

- 行面板语法

    `new RowPanel("hoho"){}` 

    特殊性提示：
    - 行面板的使用方法和面板几乎一样，除了不能添加**面板换行**
    - 行面板的用途只要是区分一个面板的内的不同区域
    - 最佳用途为 N × N 个按钮的排列


5. 现在代码类似这个样子：
```csharp
new Tab("插件平台")
{
    new Panel("测试1")
    {
        {name, cmd, image, toolTip, size, orient},
        {name, cmd, image, toolTip, size, orient},
        {RibbonSeparatorStyle.Line},
        {name, cmd, image, toolTip, size, orient},
        {name, cmd, image, toolTip, size, orient},
        new SplitButton("heihei")
        {
            {name, cmd, image, toolTip, size, orient},
            {name, cmd, image, toolTip, size, orient},
            ...
        },
        new RowPanel("hoho")
        {
            {name, cmd, image, toolTip, size, orient},
            {BreakType.Row}
            {name, cmd, image, toolTip, size, orient},
            {BreakType.Row}
            {RibbonSeparatorStyle.Space},
            {name, cmd, image, toolTip, size, orient},
            {BreakType.Row}
            {name, cmd, image, toolTip, size, orient},
            ...
        },
        ...
    },
    new Panel("测试2")
    {
        
    },
    new Panel("测试3")
    {
        
    },
    ...
}
```

在下拉按钮中只可添加普通按钮，在行面板中可以添加普通按钮/换行/分隔符/下拉按钮

放心，上述代码在vs里都有自动提示的。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
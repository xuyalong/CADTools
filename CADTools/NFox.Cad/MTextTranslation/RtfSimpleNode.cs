﻿
namespace NFox.Cad.MTextTranslation
{

    public enum RtfSimpleNodeType
    {

        Alignment,        //A
        Color,            //C
        Font,             //F
        Height,           //H
        Width,            //W
        Angle,            //Q
        Interval,         //T

    }


    public class RtfSimpleNode : RtfNode
    {

        protected readonly static string[] _typeCodes =
            new string[] { "A", "C", "F", "H", "W", "Q", "T" };

        public virtual string SpecString { get; set; }

        public RtfSimpleNode(RtfSimpleNodeType nodeType) 
        { 
            _nodeClassType = RtfNodeClassType.Simple;
            _key = (int)nodeType;
        }

        public RtfSimpleNode(RtfSimpleNodeType nodeType, string specString)
        {
            _nodeClassType = RtfNodeClassType.Simple;
            _key = (int)nodeType;
            SpecString = specString;
        }

        public override string Contents =>
            string.Format(
                "\\{0}{1};",
                _typeCodes[_key],
                SpecString);

    }





}

﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using CADTools;
using NFox.Cad.ExtendMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluginSample2.Properties;
namespace PluginSample2
{
    [Export(typeof(IPlugin))] //导出属性，用于被主程序注册为插件
    public class Test1 : PluginBase //这里要继承插件接口
    {
        
        public override RibbonTab MenuTab => new Tab("插件平台")
                {
                    new Panel("测试")
                    {
                        {"测试1","line ", Resources.setup},
                        
                    }
                };

        /// <summary>
        /// 这里就是插件的正常命令函数的编写，和正常的没啥区别
        /// </summary>
        [CommandMethod("test12")]
        public static void test12()
        {
            
            Ed.WriteMessage("this is the 2\n");
        }
    }
}

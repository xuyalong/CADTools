﻿
namespace NFox.Cad.MTextTranslation
{
    public enum RtfNodeClassType
    {
        Range = 0,
        SpecCode = 1,
        Simple = 2,
        Limit = 3,
        Script = 4,
    }

    public abstract class RtfNode
    {
        protected RtfNodeClassType _nodeClassType;
        protected int _key;
        public RtfRangeNode Owner { get; set; }

        public RtfNodeClassType NodeClassType
        {
            get { return _nodeClassType; }
        }

        public int Key
        {
            get { return _key; }
        }

        public abstract string Contents { get; }

    }
}

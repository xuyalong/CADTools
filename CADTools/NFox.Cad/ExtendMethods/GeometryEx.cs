﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using NFox.Cad.Collections;
using NFox.Cad.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.ApplicationServices;
using NFox.Collections;

namespace NFox.Cad.ExtendMethods
{

    public enum PointOnRegionType
    {
        Inside,
        On,
        Outside,
        Error
    }
    
    public static class GeometryEx
    {



        #region Point&Circle

        /// <summary>
        /// 判断点与多边形的关系
        /// </summary>
        /// <param name="pts">多边形顶点集合</param>
        /// <param name="pt">点</param>
        /// <returns>点与多边形的关系</returns>
        public static PointOnRegionType PointOnRegion(this IEnumerable<Point2d> pts, Point2d pt)
        {

            //遍历点集并生成首尾连接的多边形
            var ptlst = new LoopList<Point2d>(pts);
            if (ptlst.Count < 3)
                return PointOnRegionType.Error;

            var ls2ds = new List<LineSegment2d>();
            foreach (var node in ptlst.GetNodes())
            {
                ls2ds.Add(new LineSegment2d(node.Value, node.Next.Value));
            }
            var cc2d = new CompositeCurve2d(ls2ds.ToArray());

            //在多边形上?
            if (cc2d.IsOn(pt))
                return PointOnRegionType.On;

            //在最小包围矩形外?
            var bb2d = cc2d.BoundBlock;
            if (!bb2d.Contains(pt))
                return PointOnRegionType.Outside;

            //
            bool flag = false;
            foreach (var node in ptlst.GetNodes())
            {
                var pt1 = node.Value;
                var pt2 = node.Next.Value;
                if (pt.Y < pt1.Y && pt.Y < pt2.Y)
                    continue;
                if (pt1.X < pt.X && pt2.X < pt.X)
                    continue;
                Vector2d vec = pt2 - pt1;
                double t = (pt.X - pt1.X) / vec.X;
                double y = t * vec.Y + pt1.Y;
                if (y < pt.Y && t >= 0 && t <= 1)
                    flag = !flag;
            }
            return
                flag ?
                PointOnRegionType.Inside : PointOnRegionType.Outside;

        }

        /// <summary>
        /// 判断点与多边形的关系
        /// </summary>
        /// <param name="pts">多边形顶点集合</param>
        /// <param name="pt">点</param>
        /// <returns>点与多边形的关系</returns>
        public static PointOnRegionType PointOnRegion(this IEnumerable<Point3d> pts, Point3d pt)
        {

            //遍历点集并生成首尾连接的多边形
            var ptlst = new LoopList<Point3d>(pts);
            if (ptlst.First.Value == ptlst.Last.Value)
                ptlst.RemoveLast();
            if (ptlst.Count < 3)
                return PointOnRegionType.Error;

            var ls3ds = new List<LineSegment3d>();
            foreach (var node in ptlst.GetNodes())
            {
                ls3ds.Add(new LineSegment3d(node.Value, node.Next.Value));
            }
            var cc3d = new CompositeCurve3d(ls3ds.ToArray());

            //在多边形上?
            if (cc3d.IsOn(pt))
                return PointOnRegionType.On;

            //在最小包围矩形外?
            var bb2d = cc3d.BoundBlock;
            if (!bb2d.Contains(pt))
                return PointOnRegionType.Outside;

            //
            bool flag = false;
            foreach (var node in ptlst.GetNodes())
            {
                var pt1 = node.Value;
                var pt2 = node.Next.Value;
                if (pt.Y < pt1.Y && pt.Y < pt2.Y)
                    continue;
                if (pt1.X < pt.X && pt2.X < pt.X)
                    continue;
                Vector3d vec = pt2 - pt1;
                double t = (pt.X - pt1.X) / vec.X;
                double y = t * vec.Y + pt1.Y;
                if (y < pt.Y && t >= 0 && t <= 1)
                    flag = !flag;
            }
            return
                flag ?
                PointOnRegionType.Inside : PointOnRegionType.Outside;

        }

        /// <summary>
        /// 按两点返回最小包围圆
        /// </summary>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <param name="ptlst">输出圆上的点</param>
        /// <returns></returns>
        public static CircularArc2d GetMinCircle(Point2d pt1, Point2d pt2, out LoopList<Point2d> ptlst)
        {
            ptlst = new LoopList<Point2d> { pt1, pt2 };
            return
                new CircularArc2d
                (
                    (pt1 + pt2.GetAsVector()) / 2,
                    pt1.GetDistanceTo(pt2) / 2
                );
        }

        /// <summary>
        /// 按三点返回最小包围圆
        /// </summary>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <param name="pt3"></param>
        /// <param name="ptlst">输出圆上的点</param>
        /// <returns></returns>
        public static CircularArc2d GetMinCircle(Point2d pt1, Point2d pt2, Point2d pt3, out LoopList<Point2d> ptlst)
        {

            ptlst =
                new LoopList<Point2d> { pt1, pt2, pt3 };

            //遍历各点与下一点的向量长度,找到距离最大的两个点
            double maxLength;
            LoopListNode<Point2d> maxNode =
                ptlst.GetNodes().FindByMax
                (
                    out maxLength,
                    node => node.Value.GetDistanceTo(node.Next.Value)
                );

            //以两点做最小包围圆
            LoopList<Point2d> tptlst;
            CircularArc2d ca2d =
                GetMinCircle(maxNode.Value, maxNode.Next.Value, out tptlst);

            //如果另一点属于该圆
            if (ca2d.IsIn(maxNode.Previous.Value))
            {
                //返回
                ptlst = tptlst;
                return ca2d;
            }

            //否则按三点做圆
            ptlst.SetFirst(maxNode);
            ca2d = new CircularArc2d(pt1, pt2, pt3);
            ca2d.SetAngles(0, Math.PI * 2);
            return ca2d;

        }

        /// <summary>
        /// 按四点返回最小包围圆
        /// </summary>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <param name="pt3"></param>
        /// <param name="pt4"></param>
        /// <param name="ptlst">输出圆上的点</param>
        /// <returns></returns>
        public static CircularArc2d GetMinCircle(Point2d pt1, Point2d pt2, Point2d pt3, Point2d pt4, out LoopList<Point2d> ptlst)
        {

            LoopList<Point2d> iniptlst =
                new LoopList<Point2d> { pt1, pt2, pt3, pt4 };

            ptlst = null;
            CircularArc2d ca2d = null;

            //遍历C43的组合,环链表的优势在这里
            foreach (LoopListNode<Point2d> firstNode in iniptlst.GetNodes())
            {

                //获取各组合下三点的最小包围圆
                LoopListNode<Point2d> secondNode = firstNode.Next;
                LoopListNode<Point2d> thirdNode = secondNode.Next;
                LoopList<Point2d> tptlst;
                CircularArc2d tca2d = GetMinCircle(firstNode.Value, secondNode.Value, thirdNode.Value, out tptlst);

                //如果另一点属于该圆,并且半径小于当前值就把它做为候选解
                if (tca2d.IsIn(firstNode.Previous.Value))
                {
                    if (ca2d == null || tca2d.Radius < ca2d.Radius)
                    {
                        ca2d = tca2d;
                        ptlst = tptlst;
                    }
                }
            }

            //返回直径最小的圆
            return ca2d;

        }

        public static double GetArea(Point2d ptBase, Point2d pt1, Point2d pt2)
        {
            return (pt2 - ptBase).DotProduct((pt1 - ptBase).GetPerpendicularVector());
        }

        public static bool IsClockWise(Point2d ptBase, Point2d pt1, Point2d pt2)
        {
            return GetArea(ptBase, pt1, pt2) <= 0;
        }

        public static double GetArea(Vector2d vecBase, Vector2d vec)
        {
            return vec.DotProduct(vecBase.GetPerpendicularVector());
        }

        public static bool IsClockWise(Vector2d vecBase, Vector2d vec)
        {
            return GetArea(vecBase, vec) <= 0;
        }


        #region PointList

        public static double GetArea(this IEnumerable<Point2d> pnts)
        {

            IEnumerator<Point2d> itor = pnts.GetEnumerator();
            if (!itor.MoveNext())
                throw new ArgumentNullException();

            Point2d start = itor.Current;
            Point2d p1, p2 = start;
            double area = 0;

            while (itor.MoveNext())
            {
                p1 = p2;
                p2 = itor.Current;
                area += (p1.X * p2.Y - p2.X * p1.Y);
            }

            area = (area + (p2.X * start.Y - start.X * p2.Y)) / 2.0;
            return area;

        }

        public static bool IsClockWise(this IEnumerable<Point2d> pnts)
        {
            return pnts.GetArea() <= 0;
        }


        /// <summary>
        /// 按点集返回最小包围圆
        /// </summary>
        /// <param name="pnts"></param>
        /// <param name="ptlst">输出圆上的点</param>
        /// <returns></returns>
        public static CircularArc2d GetMinCircle(this List<Point2d> pnts, out LoopList<Point2d> ptlst)
        {

            //点数较小时直接返回
            switch (pnts.Count)
            {
                case 0:
                    ptlst = null;
                    return null;
                case 1:
                    ptlst = new LoopList<Point2d> { pnts[0] };
                    return new CircularArc2d(pnts[0], 0);
                case 2:
                    return GetMinCircle(pnts[0], pnts[1], out ptlst);
                case 3:
                    return GetMinCircle(pnts[0], pnts[1], pnts[2], out ptlst);
                case 4:
                    return GetMinCircle(pnts[0], pnts[1], pnts[2], pnts[3], out ptlst);
            }

            //按前三点计算最小包围圆
            Point2d[] tpnts = new Point2d[4];
            pnts.CopyTo(0, tpnts, 0, 3);
            CircularArc2d ca2d = GetMinCircle(tpnts[0], tpnts[1], tpnts[2], out ptlst);

            //找到点集中距离圆心的最远点为第四点
            tpnts[3] = pnts.FindByMax(pnt => ca2d.Center.GetDistanceTo(pnt));

            //如果最远点属于圆结束
            while (!ca2d.IsIn(tpnts[3]))
            {
                //如果最远点不属于圆,按此四点计算最小包围圆
                ca2d = GetMinCircle(tpnts[0], tpnts[1], tpnts[2], tpnts[3], out ptlst);

                //将结果作为新的前三点
                if (ptlst.Count == 3)
                {
                    tpnts[2] = ptlst.Last.Value;
                }
                else
                {
                    //第三点取另两点中距离圆心较远的点
                    //按算法中描述的任选其中一点的话,还是无法收敛......
                    tpnts[2] =
                        tpnts.Except(ptlst)
                        .FindByMax(pnt => ca2d.Center.GetDistanceTo(pnt));
                }
                tpnts[0] = ptlst.First.Value;
                tpnts[1] = ptlst.First.Next.Value;

                //按此三点计算最小包围圆
                ca2d = GetMinCircle(tpnts[0], tpnts[1], tpnts[2], out ptlst);

                //找到点集中圆心的最远点为第四点
                tpnts[3] = pnts.FindByMax(pnt => ca2d.Center.GetDistanceTo(pnt));
            }

            return ca2d;

        }

        public static ConvexHull2d GetConvexHull(this List<Point2d> pnts)
        {
            return new ConvexHull2d(pnts);
        }

        #endregion

        #endregion

        #region Ucs

        public static Point3d Ucs2Wcs(this Point3d point)
        {
            return point.TransformBy(SystemManager.Editor.CurrentUserCoordinateSystem);
        }

        public static Point3d Wcs2Ucs(this Point3d point)
        {
            return point.TransformBy(SystemManager.Editor.CurrentUserCoordinateSystem.Inverse());
        }

        public static Vector3d Ucs2Wcs(this Vector3d vec)
        {
            return vec.TransformBy(SystemManager.Editor.CurrentUserCoordinateSystem);
        }

        public static Vector3d Wcs2Ucs(this Vector3d vec)
        {
            return vec.TransformBy(SystemManager.Editor.CurrentUserCoordinateSystem.Inverse());
        }

        //[DllImport("acad.exe", CallingConvention = CallingConvention.Cdecl)]
        //private static extern int acedTrans(
        //    double[] point,
        //    IntPtr fromResbuf,
        //    IntPtr toResbuf,
        //    int displacement,
        //    double[] result
        //);


        //private static double[] Trans(double[] point, CoordinateSystemCode from, CoordinateSystemCode to)
        //{
        //    ResultBuffer rbfrom =
        //        new ResultBuffer(new TypedValue((int)LispDataType.Int16, from));
        //    ResultBuffer rbto =
        //        new ResultBuffer(new TypedValue((int)LispDataType.Int16, to));
        //    double[] res = new double[2];
        //    acedTrans
        //    (
        //        point.ToArray(),
        //        rbfrom.UnmanagedObject,
        //        rbto.UnmanagedObject,
        //        0,
        //        res
        //    );
        //    return res;
        //}

        public static Point3d Trans(this Point3d point, CoordinateSystemCode from, CoordinateSystemCode to)
        {
            return SystemManager.Editor.GetMatrix(from, to) * point;
        }

        public static Vector3d Trans(this Vector3d vec, CoordinateSystemCode from, CoordinateSystemCode to)
        {
            return vec.TransformBy(SystemManager.Editor.GetMatrix(from, to));
        }

        public static Point3d Wcs2Dcs(this Point3d point, bool atPaperSpace)
        {
            return
                Trans(
                    point,
                    CoordinateSystemCode.Wcs, atPaperSpace ? CoordinateSystemCode.PDcs : CoordinateSystemCode.MDcs
                );
        }

        public static Vector3d Wcs2Dcs(this Vector3d vec, bool atPaperSpace)
        {
            return
                Trans(
                    vec,
                    CoordinateSystemCode.Wcs, atPaperSpace ? CoordinateSystemCode.PDcs : CoordinateSystemCode.MDcs
                );

        }

        #endregion

        #region CompareTo

        public static int CompareTo(this Point3d p1, Point3d p2)
        {
            return 
                (p1.X == p2.X) ? 
                    ((p1.Y == p2.Y) ? 
                        p1.Z.CompareTo(p2.Z) : 
                        p1.Y.CompareTo(p2.Y)) : 
                    p1.X.CompareTo(p2.X);
        }

        public static int CompareTo(this Vector3d v1, Vector3d v2)
        {
            return 
                (v1.X == v2.X) ? 
                    ((v1.Y == v2.Y) ? 
                        v1.Z.CompareTo(v2.Z) : 
                        v1.Y.CompareTo(v2.Y)) : 
                    v1.X.CompareTo(v2.X);
        }

        public static int CompareTo(this Point2d p1, Point2d p2)
        {
            return (p1.X == p2.X) ? p1.Y.CompareTo(p2.Y) : p1.X.CompareTo(p2.X);
        }

        public static int CompareTo(this Vector2d v1, Vector2d v2)
        {
            return (v1.X == v2.X) ? v1.Y.CompareTo(v2.Y) : v1.X.CompareTo(v2.X);
        }

        #endregion

        /// <summary>
        /// 返回不等比例变换矩阵
        /// </summary>
        /// <param name="point">基点</param>
        /// <param name="x">x方向比例</param>
        /// <param name="y">y方向比例</param>
        /// <param name="z">z方向比例</param>
        /// <returns></returns>
        public static Matrix3d GetScaleMatrix(this Point3d point, double x, double y, double z)
        {
            double[] matdata = new double[16];
            matdata[0] = x;
            matdata[3] = point.X * (1 - x);
            matdata[5] = y;
            matdata[7] = point.Y * (1 - y);
            matdata[10] = z;
            matdata[11] = point.Z * (1 - z);
            matdata[15] = 1;
            return new Matrix3d(matdata);
        }

        public static Size GetSize(this Extents3d ext)
        {
            int width = (int)Math.Floor(ext.MaxPoint.X - ext.MinPoint.X);
            int height = (int)Math.Ceiling(ext.MaxPoint.Y - ext.MinPoint.Y);
            return new Size(width, height);
        }

    }
}

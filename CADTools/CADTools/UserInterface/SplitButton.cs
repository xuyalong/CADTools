﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls;
using Autodesk.Windows;
using System.Windows.Media;
using NFox.Cad.ExtendMethods;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace CADTools
{
    public class SplitButton : RibbonSplitButton, IEnumerable<RibbonItem>
    {
        /// <summary>
        /// 初始化下拉按钮
        /// </summary>
        /// <param name="text">按钮的名字</param>
        /// <param name="size">按钮的大小，默认为大</param>
        /// <param name="orientation">文字相对图标的位置，默认为右侧</param>
        public SplitButton(string text, RibbonItemSize size = RibbonItemSize.Large, Orientation orientation = Orientation.Horizontal)
        {
            Text = text;
            ShowText = true;
            Size = size;
            ShowImage = true;
            Orientation = orientation;
            Id = $"ID_Split_{text}";
        }
        public void Add(string name, string cmd, Bitmap image = null, RibbonToolTip toolTip = null,
           RibbonItemSize size = RibbonItemSize.Large, Orientation orient = Orientation.Vertical)
        {
            BitmapImage imagetmp;
            if (image == null)
            {
                imagetmp = null;
            }
            else
            {
                imagetmp = image.ToBitmapImage();
            }
            Items.Add(
                new RibbonButton
                {
                    Name = name,//按钮的名称
                    Text = name,
                    Id = $"ID_Button_{name}",
                    ShowText = true, //显示文字

                    Size = size, //按钮尺寸
                    Orientation = orient, //按钮排列方式
                    CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
                    CommandParameter = cmd + " ",
                    ShowImage = true, //显示图片
                    ToolTip = toolTip,
                    Image = imagetmp,
                    LargeImage = imagetmp
                });
        }

        public IEnumerator<RibbonItem> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }
    }
}

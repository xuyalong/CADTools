﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using NFox.Cad.Collections;
using NFox.Cad.ExtendMethods;
using Autodesk.AutoCAD.Geometry;

namespace CADTools
{
    
    public static class class1
    {
        
        public static Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
        

       

        [CommandMethod("TestMenu")]
        public static void TestMenu()
        {


            //List<MenuData> menuDatas = new List<MenuData>
            //{
            //    new MenuData
            //    {
            //        Label = "dakai",
            //        Macro = "line",
            //        HelpString= "",
            //        SubItem = null
            //    },
            //    new MenuData
            //    {
            //        Label = "open",
            //        Macro="line",
            //        HelpString="",
            //        SubItem=null
            //    }

            //};

            //ToolBarData toolBarData = new ToolBarData
            //{
            //    ToolBarName = "test",
            //    ToolButtonDatas = new List<ToolButtonData>
            //    {
            //        new ToolButtonData
            //        {
            //            Name = "opne",
            //            Macro = "line",
            //        },
            //         new ToolButtonData
            //        {
            //            Name = "--",
            //            Macro = "hah"
            //        },
            //         new ToolButtonData
            //        {
            //            Name = "hadd",
            //            Macro = "line",
            //        },
            //         new ToolButtonData
            //        {
            //            Name = "--",
            //            Macro = "hah"
            //        },
            //        new ToolButtonData
            //        {
            //            Name = "opne1",
            //            Macro = "line",
            //        }

            //    }
            //};

            //Menu.AddToolBars(toolBarData);
            //PluginBase pluginBase = new PluginBase();
            //pluginBase.AddMenu("test", menuDatas);
            



            //RibbonControl ribbonCtrl = ComponentManager.Ribbon;
            //RibbonPanelSource source = ribbonCtrl.AddTab("test", "testid", false).AddPanel("test1");
            //RibbonButton button = source.AddButton("zhix", "line");
            //string imgFileName = Path.Combine(Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath), "line.png");
            //button.SetImage(imgFileName);
            //source.AddSep(RibbonSeparatorStyle.Line);
            //RibbonSplitButton butt = source.AddSplitButton("hah");
            //butt.SetImage(imgFileName);
            //butt.AddButton("ahdh", "line").SetImage(imgFileName).SetToolTip();
            //butt.AddButton("EURG", "line").SetImage(imgFileName);
            //var panel = source.AddRowPanel();
            //panel.AddButton("ah3dh", "line");
            //panel.AddButton("ahd3h", "line");

            //source.AddPanelBreak();
            //source.AddButton("zhi2x", "line");
            //source.AddButton("z2hix", "line");



            //Tab item = new Tab("hahaha")
            //{
            //    new Panel("nihao")
            //    {
            //        {"nihao", "line"}, //普通的按钮
            //        {BreakType.Panel}, //panelbreak
            //        {RibbonSeparatorStyle.Spacer}, // 分隔符
            //        new SplitButton("nihao1") //下拉按钮
            //        {
            //            {"nihaod","line" }
            //        },
            //        new RowPanel("nihao2") //行面板
            //        {
            //            {"nihaoddd","line" },
            //            {BreakType.Row }, //换行
            //            {RibbonSeparatorStyle.Line }
            //        }
            //    },
            //    new Panel("nuhao")
            //    {
            //        {"buhao", "line"}, //普通的按钮
            //        new SplitButton("nihao4") //下拉按钮
            //        {
            //             {"nihaod3","line" },
            //            {"nihaod2","line" }
            //        },
            //        {"buhaeo", "line"}, //普通的按钮
            //    },
            //};
            //var test = new test();
            //test.addribbonmenu(item);
            //Menu.AddPopMenu(item);

            //ed.WriteMessage(item.Title);

            //var tab = new RibbonTab
            //{
            //    Title = "nihao",
            //    Id = "nihao",


            //};

            //var sou = new RibbonPanelSource { Title = "nihao" };
            //sou.Items.Add(new RibbonButton
            //{

            //    Name = "nihao",//按钮的名称
            //    Text = "nihao",
            //    ShowText = true, //显示文字

            //    Size = RibbonItemSize.Large, //按钮尺寸
            //    Orientation = Orientation.Horizontal, //按钮排列方式
            //    CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
            //    CommandParameter = "line" + " ",
            //    ShowImage = true
            //});

            //tab.Panels.Add(
            //    new RibbonPanel
            //    {
            //        Source = sou
            //    });
            //ribbonCtrl.Tabs.Add(tab);
            //ribbonCtrl.Tabs.Add(item);


            Tab tab =
               new Tab("插件平台")
               {
                    new Panel("设置")
                    {
                        {"设置","line ",Properties.Resources.setup}
                    }
               };



            Menu.AddMenu(tab);
        }
    }

    class CreateEnt
    {
        public static void AddLine(Point3d ptStart, Point3d ptEnd)
        {

            // 获取当前文档和数据库
            Document acDoc = Application.DocumentManager.MdiActiveDocument;
            Database acCurDb = acDoc.Database;
            // 启动事务
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {
                // 以读模式打开Block表
                BlockTable acBlkTbl;
                acBlkTbl = acTrans.GetObject(acCurDb.BlockTableId,
                OpenMode.ForRead) as BlockTable;
                // 以写模式打开Block表记录Model空间
                BlockTableRecord acBlkTblRec;
                acBlkTblRec = acTrans.GetObject(acBlkTbl[BlockTableRecord.ModelSpace],
                OpenMode.ForWrite) as BlockTableRecord;
                Line acLine = new Line(ptStart, ptEnd);

                // 将新对象添加到Model空间并进行事务登记
                acBlkTblRec.AppendEntity(acLine);
                acTrans.AddNewlyCreatedDBObject(acLine, true);
                // 提交修改并关闭事务
                acTrans.Commit();
            }
        }
        [CommandMethod("testaddline")]
        public static void testaddline()
        {
            Point3d ptStart = new Point3d(0, 0, 0);
            Point3d ptEnd = new Point3d(100, 100, 0);
            AddLine(ptStart, ptEnd);
        }
    }
}

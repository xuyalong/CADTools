﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;


namespace NFox.Cad.ExtendMethods
{
    public static class EntityEx
    {

        #region Vertex

        /// <summary>
        /// 获取复合实体的子实体集合
        /// </summary>
        /// <typeparam name="T">复合实体类型(PolyLine2d/3d、PolyfaceMesh......)</typeparam>
        /// <param name="ent">复合实体</param>
        /// <param name="tr">事务实例</param>
        /// <returns>子实体迭代器</returns>
        public static IEnumerable<Vertex> GetVertexs<T>(this T ent, Transaction tr) where T : Entity, IEnumerable
        {
            
            foreach (ObjectId id in ent)
            {
                yield return (Vertex)tr.GetObject(id, OpenMode.ForRead);
            }

        }

        /// <summary>
        /// 获取复合实体的子实体集合
        /// </summary>
        /// <typeparam name="T">复合实体类型(PolyLine2d/3d、PolyfaceMesh......)</typeparam>
        /// <param name="ent">复合实体</param>
        /// <param name="tr">事务实例</param>
        /// <param name="mode">打开模式(读/写)</param>
        /// <returns>子实体迭代器</returns>
        public static IEnumerable<Vertex> GetVertexs<T>(this T ent, Transaction tr, OpenMode mode) where T : Entity, IEnumerable
        {
            foreach (ObjectId id in ent)
            {
                yield return (Vertex)tr.GetObject(id, mode);
            }

        }

        public static IEnumerable<Point3d> GetPoints(this Polyline2d pl2d, Transaction tr)
        {
            foreach (ObjectId id in pl2d)
            {
                yield return ((Vertex2d)tr.GetObject(id, OpenMode.ForRead)).Position;
            }
        }

        public static IEnumerable<Point3d> GetPoints(this Polyline3d pl3d, Transaction tr)
        {
            foreach (ObjectId id in pl3d)
            {
                yield return ((PolylineVertex3d)tr.GetObject(id, OpenMode.ForRead)).Position;
            }
        }

        public static IEnumerable<Point3d> GetPoints(this Polyline pl)
        {
            return 
                Enumerable
                .Range(0,pl.NumberOfVertices)
                .Select(i => pl.GetPoint3dAt(i));
        }

        #endregion

        #region GetGroup

        public static IEnumerable<Group> GetGroups(this Entity ent, Transaction tr)
        {
            return
                ent
                .GetPersistentReactorIds()
                .Cast<ObjectId>()
                .GetObject(tr)
                .OfType<Group>();
        }

        public static IEnumerable<ObjectId> GetGroupIds(this Entity ent)
        {
            return
                ent
                .GetPersistentReactorIds()
                .Cast<ObjectId>();
        }

        #endregion

        #region TransformBy

        /// <summary>
        /// 移动实体
        /// </summary>
        /// <param name="ent">实体</param>
        /// <param name="from">基点</param>
        /// <param name="to">目标点</param>
        public static void Move(this Entity ent, Point3d from, Point3d to)
        {
            ent.TransformBy(Matrix3d.Displacement(to - from));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ent"></param>
        /// <param name="center"></param>
        /// <param name="scaleValue"></param>
        public static void Scale(this Entity ent, Point3d center, double scaleValue)
        {
            ent.TransformBy(Matrix3d.Scaling(scaleValue, center));
        }

        /// <summary>
        /// 旋转实体
        /// </summary>
        /// <param name="ent">实体</param>
        /// <param name="center">旋转中心</param>
        /// <param name="angle">转角</param>
        /// <param name="normal">旋转平面的法向矢量</param>
        public static void Rotation(this Entity ent, Point3d center, double angle, Vector3d normal)
        {
            ent.TransformBy(Matrix3d.Rotation(angle, normal, center));
        }

        /// <summary>
        /// 在XY平面内旋转实体
        /// </summary>
        /// <param name="ent">实体</param>
        /// <param name="center">旋转中心</param>
        /// <param name="angle">转角</param>
        public static void Rotation(this Entity ent, Point3d center, double angle)
        {
            ent.TransformBy(Matrix3d.Rotation(angle, Vector3d.ZAxis.TransformBy(ent.Ecs) , center));
        }

        /// <summary>
        /// 按对称轴镜像实体
        /// </summary>
        /// <param name="ent">实体</param>
        /// <param name="startPoint">对称轴起点</param>
        /// <param name="endPoint">对称轴终点</param>
        public static void Mirror(this Entity ent, Point3d startPoint, Point3d endPoint)
        {
            ent.TransformBy(Matrix3d.Mirroring(new Line3d(startPoint, endPoint)));
        }

        /// <summary>
        /// 按对称面镜像实体
        /// </summary>
        /// <param name="ent">实体</param>
        /// <param name="plane">对称平面</param>
        public static void Mirror(this Entity ent, Plane plane)
        {
            ent.TransformBy(Matrix3d.Mirroring(plane));
        }

        /// <summary>
        /// 按对称点镜像实体
        /// </summary>
        /// <param name="ent">实体</param>
        /// <param name="basePoint">对称点</param>
        public static void Mirror(this Entity ent, Point3d basePoint)
        {
            ent.TransformBy(Matrix3d.Mirroring(basePoint));
        }

        /// <summary>
        /// 更正单行文字的镜像属性
        /// </summary>
        /// <param name="txt">单行文字</param>
        public static void ValidateMirror(this DBText txt)
        {
            if (!txt.Database.Mirrtext)
            {
                txt.IsMirroredInX = false;
                txt.IsMirroredInY = false;
            }
        }

        #endregion

        /// <summary>
        /// 获取实体集合的范围
        /// </summary>
        /// <param name="ents">实体迭代器</param>
        /// <returns>实体集合的范围</returns>
        public static Extents3d GetExtents(this IEnumerable<Entity> ents)
        {
            var it = ents.GetEnumerator();
            var ext = it.Current.GeometricExtents;
            while (it.MoveNext())
                ext.AddExtents(it.Current.GeometricExtents);
            return ext;
        }

        public static void ExplodeFragments<T>(this MText mt, T obj, Func<MTextFragment, T, MTextFragmentCallbackStatus> mTextFragmentCallback)
        {
            mt.ExplodeFragments(
                (f, o) => mTextFragmentCallback(f, (T)o),
                obj);
        }

        public static string GetUnFormatString(this MText mt)
        {
            List<string> strs = new List<string>();
            mt.ExplodeFragments(
                strs,
                (f, o) =>
                {
                    o.Add(f.Text);
                    return MTextFragmentCallbackStatus.Continue;
                });
            return string.Join("", strs.ToArray());
        }

    }
}

﻿using System;

namespace NFox.Cad.Runtime
{
    [Serializable]
    public class CommandInfo : IComparable<CommandInfo>
    {

        public string GlobalName;
        public string LocalizedName;
        public string GroupName;

        public string TypeName;
        public string MethodName;

        public string Description;

        public CommandInfo() { }

        public CommandInfo(string globalName)
        {
            GlobalName = globalName;
        }

        int IComparable<CommandInfo>.CompareTo(CommandInfo other)
        {
            return this.GlobalName.CompareTo(other.GlobalName);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.DatabaseServices.Filters;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

using NFox.Cad.Collections;
using NFox.Cad.ExtendMethods;



namespace NFox.Cad.Trans
{

    public class DBTransaction : IDisposable
    {

        private bool _commit = false;

        //属性
        #region Properties

        /// <summary>
        /// 文档对象是否存在
        /// </summary>
        public bool HasDocument
        { get; private set; }

        /// <summary>
        /// 当前文档对象
        /// </summary>
        public Document Document
        { get; private set; }

        /// <summary>
        /// 当前编辑对象
        /// </summary>
        public Editor Editor
        { get; private set; }


        /// <summary>
        /// 当前数据库
        /// </summary>
        public Database Database
        { get; private set; }

        #endregion

        //事务相关
        #region Trans


        private Transaction _tr;
        /// <summary>
        /// 事务,默认Transaction
        /// </summary>
        internal Transaction Transaction
        {
            get { return _tr ?? (_tr = Database.TransactionManager.StartTransaction()); }
        }

        /// <summary>
        /// 开始一个Transaction
        /// </summary>
        public void Start()
        {
            if (_tr == null)
                _tr = Database.TransactionManager.StartTransaction();
        }

        /// <summary>
        /// 开始一个OpenCloseTransaction
        /// </summary>
        public void StartOpenClose()
        {
            if (_tr == null)
                _tr = Database.TransactionManager.StartOpenCloseTransaction();
        }

        /// <summary>
        /// 事务初始化,并只读方式打开块表
        /// </summary>
        private void Initialize(bool hasDocument)
        {

            if (HasDocument = hasDocument)
            {
                Document = Application.DocumentManager.GetDocument(Database);
                Editor = Document.Editor;
            }

        }

        /// <summary>
        /// 创建当前活动文档的事务(默认提交)
        /// </summary>
        public DBTransaction()
        {
            Database = HostApplicationServices.WorkingDatabase;
            Initialize(true);
        }

        /// <summary>
        /// 创建当前活动文档的事务
        /// </summary>
        /// <param name="commit"></param>
        public DBTransaction(bool commit)
        {
            _commit = !commit;
            Database = HostApplicationServices.WorkingDatabase;
            Initialize(true);

        }

        /// <summary>
        /// 创建指定数据库的事务,一般用于临时数据库(默认提交)
        /// </summary>
        /// <param name="database"></param>
        public DBTransaction(Database database)
        {
            Database = database;
            Initialize(false);
        }

        /// <summary>
        /// 创建指定数据库的事务
        /// </summary>
        /// <param name="database"></param>
        /// <param name="commit"></param>
        public DBTransaction(Database database, bool commit)
        {
            _commit = !commit;
            Database = database;
            Initialize(false);
        }

        /// <summary>
        /// 创建临时数据库的事务,并读入指定的文档(默认提交)
        /// </summary>
        /// <param name="fileName"></param>
        public DBTransaction(string fileName)
        {
            Database = new Database(false, true);
            Database.ReadDwgFile(fileName, FileShare.Read, true, null);

            Database.CloseInput(true);
            Initialize(false);
        }

        /// <summary>
        /// 创建临时数据库的事务,并读入指定的文档
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="commit"></param>
        public DBTransaction(string fileName, bool commit)
        {
            _commit = !commit;
            Database = new Database(false, true);
            Database.ReadDwgFile(fileName, FileShare.Read, true, null);
            Database.CloseInput(true);
            Initialize(false);
        }

        /// <summary>
        /// 销毁
        /// </summary>
        void IDisposable.Dispose()
        {
            Commit();
            Transaction.Dispose();
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        public void Commit()
        {
            if (!_commit)
            {
                Transaction.Commit();
                _commit = true;
            }
        }

        /// <summary>
        /// 撤销事务
        /// </summary>
        public void Abort()
        {
            Transaction.Abort();
        }

        /// <summary>
        /// 隐式转换为Transaction
        /// </summary>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static implicit operator Transaction(DBTransaction tr)
        {
            return tr.Transaction;
        }

        #endregion

        //对象获取
        #region GetObject

        private T GetObject<T>(T obj, ObjectId id) where T : DBObject
        {
            return obj ?? (obj = GetObject<T>(id));
        }

        /// <summary>
        /// 从句柄字符串中获取Id
        /// </summary>
        /// <param name="handleString"></param>
        /// <returns></returns>
        public ObjectId GetObjectId(string handleString)
        {
            long l = Convert.ToInt64(handleString, 16);
            Handle handle = new Handle(l);
            return Database.GetObjectId(false, handle, 0);
        }

        /// <summary>
        ///  获取对象
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <param name="openErased"></param>
        /// <returns></returns>
        public DBObject GetObject(ObjectId id, OpenMode mode, bool openErased)
        {
            return Transaction.GetObject(id, mode, openErased);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public DBObject GetObject(ObjectId id, OpenMode mode)
        {
            return Transaction.GetObject(id, mode, false);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBObject GetObject(ObjectId id)
        {
            return Transaction.GetObject(id, OpenMode.ForRead, false);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <param name="openErased"></param>
        /// <returns></returns>
        public T GetObject<T>(ObjectId id, OpenMode mode, bool openErased) where T : DBObject
        {
            return (T)Transaction.GetObject(id, mode, openErased);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetObject<T>(ObjectId id) where T : DBObject
        {
            return (T)Transaction.GetObject(id, OpenMode.ForRead, false);
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public T GetObject<T>(ObjectId id, OpenMode mode) where T : DBObject
        {
            return (T)Transaction.GetObject(id, mode, false);
        }

        #endregion

        //符号表
        #region SymbolTable
        
        
            private SymbolTable<TTable, TRecord> GetSymbolTable<TTable, TRecord>(ref SymbolTable<TTable, TRecord> table, ObjectId tableId)
                where TTable : SymbolTable
                where TRecord : SymbolTableRecord, new()
            {
                return table ?? (table = new SymbolTable<TTable, TRecord>(this, tableId));
            }

        //块表
        #region BlockTable

        private SymbolTable<BlockTable, BlockTableRecord> _bt = null;
        /// <summary>
        /// 块表
        /// </summary>
        public SymbolTable<BlockTable, BlockTableRecord> BlockTable
        {
            get { return GetSymbolTable(ref _bt, Database.BlockTableId); }
        }

        #endregion

        //层表
        #region LayerTable

        private SymbolTable<LayerTable, LayerTableRecord> _lt = null;
        /// <summary>
        /// 层表
        /// </summary>
        public SymbolTable<LayerTable, LayerTableRecord> LayerTable
        {
            get { return GetSymbolTable(ref _lt, Database.LayerTableId); }
        }

        #endregion

        //文字样式表
        #region TextStyleTable

        private SymbolTable<TextStyleTable, TextStyleTableRecord> _tst = null;
        /// <summary>
        /// 文字样式表
        /// </summary>
        public SymbolTable<TextStyleTable, TextStyleTableRecord> TextStyleTable
        {
            get { return GetSymbolTable(ref _tst, Database.TextStyleTableId); }
        }

        #endregion

        //注册应用程序
        #region RegAppTable

        private SymbolTable<RegAppTable, RegAppTableRecord> _rat = null;
        public SymbolTable<RegAppTable, RegAppTableRecord> RegAppTable
        {
            get { return GetSymbolTable(ref _rat, Database.RegAppTableId); }
        }

        #endregion

        //标注样式表
        #region DimStyleTable

        private SymbolTable<DimStyleTable, DimStyleTableRecord> _dst = null;
        /// <summary>
        /// 标注样式表
        /// </summary>
        public SymbolTable<DimStyleTable, DimStyleTableRecord> DimStyleTable
        {
            get { return GetSymbolTable(ref _dst, Database.DimStyleTableId); }
        }

        #endregion

        //线型表
        #region LinetypeTable

        private SymbolTable<LinetypeTable, LinetypeTableRecord> _ltt = null;
        /// <summary>
        /// 线型表
        /// </summary>
        public SymbolTable<LinetypeTable, LinetypeTableRecord> LinetypeTable
        {
            get { return GetSymbolTable(ref _ltt, Database.LinetypeTableId); }
        }

        public SelectionSet SelectByLineWeight(LineWeight lineWeight)
        {

            OpFilter filter = new OpEqual(370, lineWeight);

            var lays =
                LayerTable
                .GetRecords()
                .Where(ltr => ltr.LineWeight == lineWeight)
                .Select(ltr => ltr.Name)
                .ToArray();

            if (lays.Length > 0)
            {
                filter =
                    new OpOr
                    {
                        filter,
                        new OpAnd
                        {
                            { 8, string.Join(",", lays) },
                            { 370, LineWeight.ByLayer }
                        }
                    };
            }

            PromptSelectionResult res = Editor.SelectAll(filter);
            return res.Value;

        }
        #endregion

        //用户坐标系
        #region UcsTable

        private SymbolTable<UcsTable, UcsTableRecord> _ut = null;
        public SymbolTable<UcsTable, UcsTableRecord> UcsTable
        {
            get { return GetSymbolTable(ref _ut, Database.UcsTableId); }
        }

        #endregion

        //视图
        #region ViewTable

        private SymbolTable<ViewTable, ViewTableRecord> _vt = null;
        public SymbolTable<ViewTable, ViewTableRecord> ViewTable
        {
            get { return GetSymbolTable(ref _vt, Database.ViewTableId); }
        }

        #endregion

        //视口
        #region ViewportTable

        private SymbolTable<ViewportTable, ViewportTableRecord> _vpt = null;
        public SymbolTable<ViewportTable, ViewportTableRecord> ViewportTable
        {
            get { return GetSymbolTable(ref _vpt, Database.ViewportTableId); }
        }

        #endregion

        #endregion

        //字典
        #region Dictionary

        private DBDictionary _root;
        
        public DBDictionary RootDictionary
        {
            get
            {
                return _root ?? (_root = GetObject<DBDictionary>(Database.NamedObjectsDictionaryId));
            }
        }

        //保存和获取数据
        #region Value
        /// <summary>
        /// 保存数据到字典
        /// </summary>
        /// <param name="value">数据</param>
        /// <param name="dict">字典</param>
        /// <param name="key">键值</param>
        public void SetToDictionary(DBDictionary dict, string key, DBObject value)
        {
            using (dict.UpgradeOpenAndRun())
            {
                if (dict.Contains(key))
                    dict.Remove(key);
                dict.SetAt(Transaction, key, value);
            }
        }

        /// <summary>
        /// 从字典中获取数据
        /// </summary>
        /// <param name="dict">字典</param>
        /// <param name="key">键值</param>
        /// <returns>数据</returns>
        public DBObject GetFromDictionary(DBDictionary dict, string key)
        {
            if (dict != null)
            {
                if (dict.Contains(key))
                {
                    DBObject obj = GetObject(dict.GetAt(key));
                    return obj;
                }
            }
            return null;
        }

        public DBDictionary GetSubDictionary(bool createSubDictionary, params string[] dictNames)
        {
            return RootDictionary.GetSubDictionary(Transaction, createSubDictionary, dictNames);
        }

        public DBDictionary GetSubDictionary(DBDictionary dict, bool createSubDictionary, params string[] dictNames)
        {
            return dict.GetSubDictionary(Transaction, createSubDictionary, dictNames);
        }

        public DBDictionary GetSubDictionary(DBObject obj, bool createSubDictionary, params string[] dictNames)
        {
            return obj.GetSubDictionary(Transaction, createSubDictionary, dictNames);
        }

        #endregion

        //保存和获取扩展数据
        #region XRecord

        /// <summary>
        /// 保存扩展数据到字典
        /// </summary>
        /// <param name="rb">扩展数据</param>
        /// <param name="dict">字典</param>
        /// <param name="key">键值</param>
        public void SetXRecord(DBDictionary dict, string key, ResultBuffer rb)
        {
            SetToDictionary(dict, key, new Xrecord { Data = rb });
        }

        /// <summary>
        /// 从字典中获取扩展数据
        /// </summary>
        /// <param name="dict">字典</param>
        /// <param name="key">键值</param>
        /// <returns>扩展数据</returns>
        public ResultBuffer GetXRecord(DBDictionary dict, string key)
        {
            Xrecord rec = GetFromDictionary(dict, key) as Xrecord;
            if (rec != null)
                return rec.Data;
            return null;
        }

        #endregion

        //编组字典
        #region GroupDictionary

        private DBDictionary _groupDictionary = null;
        /// <summary>
        /// 编组字典
        /// </summary>
        public DBDictionary GroupDictionary
        {
            get { return GetObject(_groupDictionary, Database.GroupDictionaryId); }
        }

        /// <summary>
        /// 添加编组
        /// </summary>
        /// <param name="name">组名</param>
        /// <param name="ids">实体Id集合</param>
        /// <returns>编组Id</returns>
        public ObjectId AddGroup(string name, ObjectIdCollection ids)
        {
            if (GroupDictionary.Contains(name))
            {
                return ObjectId.Null;
            }
            else
            {
                using (GroupDictionary.UpgradeOpenAndRun())
                {
                    Group g = new Group();
                    g.Append(ids);
                    GroupDictionary.SetAt(name, g);
                    Transaction.AddNewlyCreatedDBObject(g, true);
                    return g.ObjectId;
                }
            }
        }

        /// <summary>
        /// 添加编组
        /// </summary>
        /// <param name="name">组名</param>
        /// <param name="ids">实体Id集合</param>
        /// <returns>编组Id</returns>
        public ObjectId AddGroup(string name, IEnumerable<ObjectId> ids)
        {
            if (GroupDictionary.Contains(name))
            {
                return ObjectId.Null;
            }
            else
            {
                using (GroupDictionary.UpgradeOpenAndRun())
                {
                    Group g = new Group();
                    foreach(ObjectId id in ids)
                        g.Append(id);
                    GroupDictionary.SetAt(name, g);
                    Transaction.AddNewlyCreatedDBObject(g, true);
                    return g.ObjectId;
                }
            }

        }

        /// <summary>
        /// 按选择条件获取编组集合
        /// </summary>
        /// <param name="func">选择条件</param>
        /// <returns>编组集合</returns>
        public IEnumerable<Group> GetGroups(Func<Group,bool> func)
        {
            return
                GroupDictionary
                .GetAllObjects<Group>(Transaction)
                .Where(func);
        }

        /// <summary>
        /// 返回实体的所在编组的集合
        /// </summary>
        /// <param name="ent">图元实体</param>
        /// <returns>编组集合</returns>
        public IEnumerable<Group> GetGroups(Entity ent)
        {
            return
                ent.GetPersistentReactorIds()
                .Cast<ObjectId>()
                .Select(id => GetObject(id))
                .OfType<Group>();
        }

        /// <summary>
        /// 移除所有的空组
        /// </summary>
        /// <returns>编组名称集合</returns>
        public List<string> RemoveNullGroup()
        {
            var groups = GetGroups(g => g.NumEntities < 2);
            List<string> names = new List<string>();
            foreach (Group g in groups)
            {
                g.UpgradeOpen();
                names.Add(g.Name);
                g.Erase();
            }
            return names;
        }

        public List<string> RemoveNullGroup(Func<string,bool> func)
        {
            var groups = GetGroups(g => g.NumEntities < 2);
            List<string> names = new List<string>();
            foreach (Group g in groups)
            {
                if (func(g.Name))
                {
                    names.Add(g.Name);
                    using (g.UpgradeOpenAndRun())
                    {
                        g.Erase();
                    }
                }
            }
            return names;
        }

        #endregion

        #region MLeaderStyleDictionary

        private DBDictionary _mLeaderStyleDictionary = null;
        public DBDictionary MLeaderStyleDictionary
        {
            get { return GetObject(_mLeaderStyleDictionary, Database.MLeaderStyleDictionaryId); }
        }
        
        #endregion

        #region MLStyleDictionary

        private DBDictionary _mLStyleDictionary = null;
        public DBDictionary MLStyleDictionary
        {
            get { return GetObject(_mLStyleDictionary, Database.MLStyleDictionaryId); }
        }

        #endregion

        #region MaterialDictionary

        private DBDictionary _materialDictionary = null;
        public DBDictionary MaterialDictionary
        {
            get { return GetObject(_materialDictionary, Database.MaterialDictionaryId); }
        }

        #endregion

        #region TableStyleDictionary

        private DBDictionary _tableStyleDictionary = null;
        public DBDictionary TableStyleDictionary
        {
            get { return GetObject(_tableStyleDictionary, Database.TableStyleDictionaryId); }
        }

        #endregion

        #region VisualStyleDictionary

        private DBDictionary _visualStyleDictionary = null;
        public DBDictionary VisualStyleDictionary
        {
            get { return GetObject(_visualStyleDictionary, Database.VisualStyleDictionaryId); }
        }

        #endregion

        #region ColorDictionary

        private DBDictionary _colorDictionary = null;
        public DBDictionary ColorDictionary
        {
            get { return GetObject(_colorDictionary, Database.ColorDictionaryId); }
        }

        #endregion

        #region PlotSettingsDictionary

        private DBDictionary _plotSettingsDictionary = null;
        public DBDictionary PlotSettingsDictionary
        {
            get { return GetObject(_plotSettingsDictionary, Database.PlotSettingsDictionaryId); }
        }

        #endregion

        #region PlotStyleNameDictionary

        private DBDictionary _plotStyleNameDictionary = null;
        public DBDictionary PlotStyleNameDictionary
        {
            get { return GetObject(_plotStyleNameDictionary, Database.PlotStyleNameDictionaryId); }
        }

        #endregion
        
        #region LayoutDictionary

        private DBDictionary _layoutDictionary = null;
        public DBDictionary LayoutDictionary
        {
            get { return GetObject(_layoutDictionary, Database.LayoutDictionaryId); }
        }

        #endregion

        #endregion

        //块表记录
        #region BlockTableRecord
        /// <summary>
        /// 获取块表记录
        /// </summary>
        /// <param name="id">块表记录Id</param>
        /// <param name="openmode">打开模式</param>
        /// <returns>块表记录</returns>
        public BlockTableRecord OpenBlockTableRecord(ObjectId id, OpenMode openmode)
        {
            return BlockTable.GetRecord(id, openmode);
        }

        /// <summary>
        /// 获取块表记录
        /// </summary>
        /// <param name="id">块表记录Id</param>
        /// <returns>块表记录</returns>
        public BlockTableRecord OpenBlockTableRecord(ObjectId id)
        {
            return BlockTable.GetRecord(id);
        }

        /// <summary>
        /// 获取块表记录
        /// </summary>
        /// <param name="name">块表记录名</param>
        /// <param name="openmode">打开模式</param>
        /// <returns>块表记录</returns>
        public BlockTableRecord OpenBlockTableRecord(string name, OpenMode openmode)
        {
            return BlockTable.GetRecord(name, openmode);
        }

        /// <summary>
        /// 获取块表记录
        /// </summary>
        /// <param name="name">块表记录名</param>
        /// <returns>块表记录</returns>
        public BlockTableRecord OpenBlockTableRecord(string name)
        {
            return BlockTable.GetRecord(name);
        }

        /// <summary>
        /// 获取当前块表记录
        /// </summary>
        /// <param name="openmode">打开模式</param>
        /// <returns>块表记录</returns>
        public BlockTableRecord OpenCurrentSpace(OpenMode openmode)
        {
            return BlockTable.GetRecord(Database.CurrentSpaceId, openmode);
        }

        /// <summary>
        /// 获取当前块表记录
        /// </summary>
        /// <returns>块表记录</returns>
        public BlockTableRecord OpenCurrentSpace()
        {
            return BlockTable.GetRecord(Database.CurrentSpaceId);
        }

        /// <summary>
        /// 获取图纸空间
        /// </summary>
        /// <param name="openmode">打开模式</param>
        /// <returns>图纸空间</returns>
        public BlockTableRecord OpenPaperSpace(OpenMode openmode)
        {
            return 
                GetObject<BlockTableRecord>(
                    BlockTable.AcTable[BlockTableRecord.PaperSpace],  
                    openmode);
        }

        /// <summary>
        /// 获取图纸空间
        /// </summary>
        /// <returns>图纸空间</returns>
        public BlockTableRecord OpenPaperSpace()
        {
            return OpenPaperSpace(OpenMode.ForRead);
        }

        /// <summary>
        /// 获取模型空间
        /// </summary>
        /// <param name="openmode">打开模式</param>
        /// <returns>模型空间</returns>
        public BlockTableRecord OpenModelSpace(OpenMode openmode)
        {
            return
                GetObject<BlockTableRecord>(
                    BlockTable.AcTable[BlockTableRecord.ModelSpace],
                    openmode);
        }

        /// <summary>
        /// 获取模型空间
        /// </summary>
        /// <returns>模型空间</returns>
        public BlockTableRecord OpenModelSpace()
        {
            return OpenModelSpace(OpenMode.ForRead);
        }


        #endregion

        //添加实体
        #region Add Entity
        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="btr">块表记录</param>
        /// <param name="entity">图元实体</param>
        /// <returns>实体Id</returns>
        public ObjectId AddEntity(BlockTableRecord btr, Entity entity)
        {
            return btr.AddEntity(Transaction, entity);
        }

        /// <summary>
        /// 添加实体集合
        /// </summary>
        /// <param name="btr">块表记录</param>
        /// <param name="ents">图元实体集合</param>
        /// <returns>实体Id集合</returns>
        public List<ObjectId> AddEntity(BlockTableRecord btr, DBObjectCollection ents)
        {
            return btr.AddEntity(Transaction, ents);
        }

        /// <summary>
        /// 添加实体集合
        /// </summary>
        /// <param name="btr">块表记录</param>
        /// <param name="ents">图元实体集合</param>
        /// <returns>实体Id集合</returns>
        public List<ObjectId> AddEntity<T>(BlockTableRecord btr, IEnumerable<T> ents) where T : Entity
        {
            return btr.AddEntity(Transaction, ents);
        }

        #endregion

        //删除实体
        #region Erase Entity
        /// <summary>
        /// 删除实体集合
        /// </summary>
        /// <param name="ids">实体Id集合</param>
        /// <returns></returns>
        public bool Erase(ObjectIdCollection ids)
        {
            try
            {
                foreach (ObjectId id in ids)
                {
                    DBObject obj;
                    obj = GetObject(id, OpenMode.ForWrite);
                    obj.Erase(true);
                }
                return true;
            }
            catch
            { }
            return false;
        }

        /// <summary>
        /// 删除实体集合
        /// </summary>
        /// <param name="ids">实体Id集合</param>
        /// <returns></returns>
        public bool Erase(IEnumerable<ObjectId> ids)
        {
            try
            {
                foreach (ObjectId id in ids)
                {
                    DBObject obj;
                    obj = GetObject(id, OpenMode.ForWrite);
                    obj.Erase(true);
                }
                return true;
            }
            catch
            { }
            return false;
        }

        /// <summary>
        /// 删除实体集合
        /// </summary>
        /// <param name="ids">实体Id集合</param>
        /// <returns></returns>
        public bool Erase<T>(IEnumerable<T> ents) where T :DBObject
        {
            try
            {
                foreach (T ent in ents)
                {
                    using (ent.UpgradeOpenAndRun())
                    {
                        ent.Erase(true);
                    }
                }
                return true;
            }
            catch
            { }
            return false;
        }

        public void Clear(BlockTableRecord btr)
        {
            foreach (ObjectId id in btr)
            {
                DBObject obj = GetObject(id, OpenMode.ForWrite);
                obj.Erase();
            }
        }

        #endregion

        //块参照
        #region BlockReference

        //添加属性到块参照
        #region AppendAttribToBlock

        /// <summary>
        /// 添加属性到块参照
        /// </summary>
        /// <param name="blkrefid">块参照Id</param>
        /// <param name="atts">属性集合</param>
        /// <returns>属性定义和属性参照对照表</returns>
        public Dictionary<AttributeDefinition, AttributeReference> 
            AppendAttribToBlock(ObjectId blkrefid, List<string> atts)
        {
            BlockReference blkref = GetObject<BlockReference>(blkrefid, OpenMode.ForWrite);
            return AppendAttribToBlock(blkref, atts);
        }

        /// <summary>
        /// 添加属性到块参照
        /// </summary>
        /// <param name="blkref">块参照</param>
        /// <param name="atts">属性集合</param>
        /// <returns>属性定义和属性参照对照表</returns>
        public Dictionary<AttributeDefinition, AttributeReference> 
            AppendAttribToBlock(BlockReference blkref, List<string> atts)
        {

            var blkdef = GetObject<BlockTableRecord>(blkref.BlockTableRecord);

            int i = 0;
            if (blkdef.HasAttributeDefinitions)
            {
                var attribs =
                    new Dictionary<AttributeDefinition, AttributeReference>();

                var attdefs = 
                    blkdef.GetEntities<AttributeDefinition>(Transaction)
                    .Where(attdef => !(attdef.Constant || attdef.Invisible));

                foreach (var attdef in attdefs)
                {
                    AttributeReference attref = new AttributeReference();
                    attref.SetAttributeFromBlock(attdef, blkref.BlockTransform);
                    if (i < atts.Count)
                        attref.TextString = atts[i];
                    else
                        attref.TextString = attdef.TextString;
                    i++;
                    blkref.AttributeCollection.AppendAttribute(attref);
                    Transaction.AddNewlyCreatedDBObject(attref, true);
                    attribs.Add(attdef, attref);
                }
                return attribs;
            }
            return null;
        }

        /// <summary>
        /// 添加属性到块参照
        /// </summary>
        /// <param name="blkrefid">块参照Id</param>
        /// <param name="atts">属性集合</param>
        /// <returns>属性定义和属性参照对照表</returns>
        public Dictionary<AttributeDefinition, AttributeReference> 
            AppendAttribToBlock(ObjectId blkrefid, List<AttributeDefinition> atts)
        {
            BlockReference blkref = GetObject<BlockReference>(blkrefid, OpenMode.ForWrite);
            return AppendAttribToBlock(blkref, atts);
        }

        /// <summary>
        /// 添加属性到块参照
        /// </summary>
        /// <param name="blkref">块参照</param>
        /// <param name="atts">属性集合</param>
        /// <returns>属性定义和属性参照对照表</returns>
        public Dictionary<AttributeDefinition, AttributeReference> 
            AppendAttribToBlock(BlockReference blkref, List<AttributeDefinition> atts)
        {
            var attribs = 
                new Dictionary<AttributeDefinition, AttributeReference>();
            for (int i = 0; i < atts.Count; i++)
            {
                AttributeDefinition attdef = atts[i];
                AttributeReference attref = new AttributeReference();
                attref.SetAttributeFromBlock(attdef, blkref.BlockTransform);
                attref.TextString = attdef.TextString;
                blkref.AttributeCollection.AppendAttribute(attref);
                Transaction.AddNewlyCreatedDBObject(attref, true);
            }
            return attribs;
        }

        #endregion

        //动态添加块参照
        #region InsertBlockRef

        public bool DragBlockRef(BlockReference bref, List<string> atts)
        {
            BlockRefJig jig = new BlockRefJig(Editor, bref, AppendAttribToBlock(bref, atts));
            PromptResult res = jig.DragByMove();
            if (res.Status == PromptStatus.OK)
            {
                res = jig.DragByRotation();
                if (res.Status == PromptStatus.OK)
                    return true;
            }
            bref.Erase();
            return false;
        }

        public bool DragBlockRef(BlockReference bref)
        {
            return DragBlockRef(bref, new List<string>());
        }

        /// <summary>
        /// 动态添加块参照
        /// </summary>
        /// <param name="bdefid">块定义Id</param>
        /// <param name="atts">属性集合</param>
        /// <returns>块参照Id</returns>
        public ObjectId InsertBlockRef(ObjectId bdefid, List<string> atts)
        {
            BlockTableRecord btr = OpenCurrentSpace();
            BlockReference blkref = new BlockReference(Point3d.Origin, bdefid);
            ObjectId id = AddEntity(btr, blkref);
            return DragBlockRef(blkref, atts) ? id : ObjectId.Null;
        }

        /// <summary>
        /// 动态添加块参照
        /// </summary>
        /// <param name="bdefid">块定义Id</param>
        /// <returns>块参照Id</returns>
        public ObjectId InsertBlockRef(ObjectId bdefid)
        {
            return InsertBlockRef(bdefid, new List<string>());
        }

        /// <summary>
        /// 动态添加块参照
        /// </summary>
        /// <param name="name">块定义名</param>
        /// <param name="atts">属性集合</param>
        /// <returns>块参照Id</returns>
        public ObjectId InsertBlockRef(string name, List<string> attribs)
        {
            return InsertBlockRef(BlockTable[name], attribs);
        }

        /// <summary>
        /// 动态添加块参照
        /// </summary>
        /// <param name="name">块定义名</param>
        /// <returns>块参照Id</returns>
        public ObjectId InsertBlockRef(string name)
        {
            return InsertBlockRef(name, new List<string>());
        }

        #endregion

        //裁剪块参照
        #region ClipBlockRef

        const string filterDictName = "ACAD_FILTER";
        const string spatialName = "SPATIAL";

        public void ClipBlockRef(BlockReference bref, IEnumerable<Point3d> pt3ds)
        {

            Matrix3d mat = bref.BlockTransform.Inverse();
            var pts = 
                pt3ds
                .Select(p => p.TransformBy(mat))
                .Select(p => new Point2d(p.X, p.Y))
                .ToCollection();

            SpatialFilterDefinition sfd = new SpatialFilterDefinition(pts, Vector3d.ZAxis, 0.0, 0.0, 0.0, true);
            SpatialFilter sf = new SpatialFilter { Definition = sfd };
            var dict = GetSubDictionary(bref, true, filterDictName);
            SetToDictionary(dict, spatialName, sf);
        }

        /// <summary>
        /// 裁剪块参照
        /// </summary>
        /// <param name="bref">块参照</param>
        /// <param name="pt1">第一角点</param>
        /// <param name="pt2">第二角点</param>
        public void ClipBlockRef(BlockReference bref, Point3d pt1, Point3d pt2)
        {

            Matrix3d mat = bref.BlockTransform.Inverse();
            pt1 = pt1.TransformBy(mat);
            pt2 = pt2.TransformBy(mat);
            Point2dCollection pts = 
                new Point2dCollection 
                { 
                    new Point2d(Math.Min(pt1.X, pt2.X), Math.Min(pt1.Y, pt2.Y)), 
                    new Point2d(Math.Max(pt1.X, pt2.X), Math.Max(pt1.Y, pt2.Y)) 
                };

            SpatialFilterDefinition sfd = new SpatialFilterDefinition(pts, Vector3d.ZAxis, 0.0, 0.0, 0.0, true);
            SpatialFilter sf = new SpatialFilter { Definition = sfd };
            var dict = GetSubDictionary(bref, true, filterDictName);
            SetToDictionary(dict, spatialName, sf);

        }

        #endregion

        #endregion

    }

}

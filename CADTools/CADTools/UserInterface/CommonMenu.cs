﻿using Autodesk.AutoCAD.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Windows;
using System.Windows.Media.Imaging;

namespace CADTools
{
    /// <summary>
    /// 菜单项数据类
    /// </summary>
    public class MenuData
    {
        public string Label { get; set; }
        public string Macro { get; set; }
        public string HelpString { get; set; }
        public List<MenuData> SubItem { get; set; }

    }

    public class ToolBarData
    {
        public string ToolBarName;
        public List<ToolButtonData> ToolButtonDatas;
    }

    public class ToolButtonData
    {
        public string Name;
        public string HelpString;
        public string Macro;
        public string SmallIconName;
        public string LargeIconName;
        public bool FlyoutButton;

    }

    public enum DockSide
    {
        Top,
        Bottom,
        Left,
        Right
    }

    public static class Menu
    {
        private static dynamic App = Application.AcadApplication; //定义cad对象
        private static dynamic MenuBar = App.MenuBar;
        private static dynamic MenuGroups = App.MenuGroups;

        #region 通用数据版API
        /// <summary>
        /// 插入菜单条
        /// </summary>
        /// <param name="PopMenu">菜单条对象</param>
        /// <param name="PopItems">菜单项列表</param>
        private static void InsertPopMenuItems(dynamic PopMenu, List<MenuData> PopItems)
        {
            //int k = 0;
            foreach (var item in PopItems)
            {
                if (item.Label == "--")
                {
                    PopMenu.AddSeparator(PopMenu.Count + 1);
                }
                else
                {
                    if (item.Label != null && item.Macro != null)
                    {
                        var menu = PopMenu.AddMenuItem(PopMenu.Count + 1, item.Label, item.Macro);
                        //menu.HelpString = item.HelpString;
                    }
                    else
                    {
                        var menu = PopMenu.AddSubMenu(PopMenu.Count + 1, item.Label);
                        if (item.SubItem != null)
                        {
                            InsertPopMenuItems(menu, item.SubItem);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 添加下拉菜单
        /// </summary>
        /// <param name="PopName">下拉菜单名称</param>
        /// <param name="PopItems">菜单项列表</param>
        /// <param name="MenuGroupName">菜单组名称</param>
        /// <param name="InsertBeforeItem">在哪个菜单之前插入，null或者找不到的插入最后</param>
        public static void AddPopMenu(string PopName, List<MenuData> PopItems, string MenuGroupName = "ACAD", string InsertBeforeItem = null)
        {
            RemoveMenuItem(PopName);
            int i;
            if (InsertBeforeItem != null)
            {
                i = MenuBar.Count - 1;
                while (i >= 0 && InsertBeforeItem != MenuBar.Item(i).Name)
                {
                    i--;
                }
                if (i < 0)
                {
                    i = MenuBar.Count;
                }
            }
            else
            {
                i = MenuBar.Count;
            }
            dynamic newMenu;
            try
            {
                newMenu = MenuGroups.Item(MenuGroupName).Menus.Item(PopName);
            }
            catch (Exception)
            {
                newMenu = MenuGroups.Item(MenuGroupName).Menus.Add(PopName);
            }


            foreach (var item in newMenu)
            {
                item.Delete();
            }
            newMenu.InsertInMenuBar(i);
            InsertPopMenuItems(newMenu, PopItems);
        }

        /// <summary>
        /// 添加工具条，由于分隔符的位置由于未知的原因不在正确的位置，不建议添加分隔符。
        /// </summary>
        /// <param name="toolBarDatas">工具条的列表</param>
        /// <param name="MenuGroupName">菜单组的名字</param>
        public static void AddToolBars(ToolBarData toolBarData, string MenuGroupName = "ACAD")
        {
            dynamic menuGroup;
            try
            {
                menuGroup = MenuGroups.Item(MenuGroupName);
            }
            catch (Exception)
            {

                throw new Exception($"菜单组 {MenuGroupName} 不存在，无法加载工具条！");
            }


            var toolBars = menuGroup.Toolbars;

            dynamic toolBar;
            try
            {
                toolBar = toolBars.Item(toolBarData.ToolBarName);
                toolBar.Delete();
            }
            catch (Exception)
            {
            }

            toolBar = toolBars.Add(toolBarData.ToolBarName);
            toolBar.Dock(2);

            foreach (var item in toolBarData.ToolButtonDatas)
            {


                if (item.Name == "--" || item.Name == null)
                {
                    try
                    {
                        toolBar.AddSeparator(toolBar.Count + 1);
                    }
                    catch (Exception)
                    {

                    }

                }
                else
                {
                    var toolBarItem = toolBar.AddToolbarButton(toolBar.Count + 1, item.Name, item.HelpString, item.Macro, item.FlyoutButton);
                    try
                    {
                        if (item.LargeIconName == null)
                        {
                            item.LargeIconName = item.SmallIconName;
                        }
                        toolBarItem.SetBitmaps(item.SmallIconName, item.LargeIconName);
                    }
                    catch (Exception)
                    {
                    }

                    if (item.FlyoutButton)
                    {
                        toolBarItem.AttachToolbarToFlyout(MenuGroupName, toolBarData.ToolBarName);
                    }
                }

            }

        }
        /// <summary>
        /// 删除下拉菜单函数
        /// </summary>
        /// <param name="PopName">菜单名称</param>
        private static void RemoveMenuItem(string PopName)
        {
            try
            {
                dynamic menu = MenuBar.Item(PopName);
                menu.RemoveFromMenuBar();
            }
            catch (Exception)
            {
            }
        }
        #endregion

        #region ribbontab数据版API

        /// <summary>
        /// 插入菜单项
        /// </summary>
        /// <param name="tab">ribbon选项卡对象</param>
        public static void AddMenu(params RibbonTab[] tab)
        {
            foreach (var tabtmp in tab)
            {
                AddPopMenu(tabtmp);
            }

        }

        /// <summary>
        /// 添加下拉菜单
        /// </summary>
        /// <param name="tab">ribbon选项卡</param>
        public static void AddPopMenu(RibbonTab tab)
        {
            //RemoveMenuItem(tab.Title);
            dynamic newMenu;

            try
            {
                dynamic tmp1 = MenuGroups.Item("ACAD");
                dynamic tmp2 = tmp1.Menus.Item(tab.Title);
                newMenu = MenuGroups.Item("ACAD").Menus.Item(tab.Title);
            }
            catch (Exception)
            {
                newMenu = MenuGroups.Item("ACAD").Menus.Add(tab.Title);
                newMenu.InsertInMenuBar(MenuBar.Count - 1);
            }
            foreach (var item in tab.Panels)
            {
                dynamic submenu = null;

                for (int i = 0; i < newMenu.Count; i++)
                {
                    dynamic sub = newMenu.Item(i);
                    if (sub.Label == item.Source.Title)
                    {
                        submenu = sub.SubMenu;
                    }
                }

                if (submenu is null)
                {
                    submenu = newMenu.AddSubMenu(newMenu.Count + 1, item.Source.Title);
                }
                InsertPopMenuItems(submenu, item.Source.Items);
            }
        }

        /// <summary>
        /// 插入菜单条
        /// </summary>
        /// <param name="PopMenu">菜单条对象</param>
        /// <param name="ribbonItems">ribbon按钮集合</param>
        private static void InsertPopMenuItems(dynamic PopMenu, RibbonItemCollection ribbonItems)
        {
            foreach (var item in ribbonItems)
            {
                if (item is RibbonSeparator)
                {
                    PopMenu.AddSeparator(PopMenu.Count + 1);
                }
                else if (item is RibbonSplitButton tmp1)
                {
                    var menu = PopMenu.AddSubMenu(PopMenu.Count + 1, tmp1.Text ?? "其他");
                    InsertPopMenuItems(menu, tmp1.Items);
                }
                else if (item is RibbonRowPanel tmp2)
                {
                    InsertPopMenuItems(PopMenu, tmp2.Items);
                }
                else if (item is RibbonButton tmp)
                {
                    PopMenu.AddMenuItem(PopMenu.Count + 1, tmp.Text, tmp.CommandParameter.ToString());
                }

            }
        }

        #region 不成熟的代码
        private static void AddToolBar(RibbonTab tab)
        {
            dynamic menuGroup = MenuGroups.Item("ACAD");
            foreach (var item in tab.Panels)
            {
                dynamic toolBar;
                try
                {
                    toolBar = menuGroup.Toolbars.Item(item.Source.Title);

                }
                catch (Exception)
                {
                    toolBar = menuGroup.Toolbars.Add(item.Source.Title);

                }
                toolBar.Dock(2);
                addToobutton(toolBar, item.Source.Items);
            }
        }

        private static void addToobutton(dynamic toolBar, RibbonItemCollection items)
        {
            foreach (var item in items)
            {
                if (item is RibbonSeparator)
                {
                    toolBar.AddSeparator(toolBar.Count + 1);
                }
                else if (item is RibbonButton tmp)
                {
                    addbaritem(toolBar, tmp);
                }
                else if (item is RibbonRowPanel tmp1)
                {
                    addToobutton(toolBar, tmp1.Items);
                }
                else if (item is RibbonSplitButton tmp2)
                {
                    var toolBarItem = addbaritem(toolBar, tmp2);
                    toolBarItem.AttachToolbarToFlyout("ACAD", addbutton(tmp2.Text, tmp2.Items));
                }
            }
        }

        private static dynamic addbaritem(dynamic toolbar, RibbonButton button)
        {
            string help;
            if (button.ToolTip != null)
            {
                help = button.ToolTip.ToString();
            }
            else
            {
                help = "";
            }
            var toolBarItem = toolbar.AddToolbarButton(toolbar.Count + 1, button.Text, help, button.CommandParameter.ToString());
            try
            {
                string path = (button.Image as BitmapImage).UriSource.LocalPath;
                string path2 = (button.LargeImage as BitmapImage).UriSource.LocalPath;
                toolBarItem.SetBitmaps(path, path2);
            }
            catch (Exception)
            {
            }
            return toolBarItem;
        }
        private static dynamic addbutton(string name, RibbonItemCollection items)
        {
            dynamic menuGroup = MenuGroups.Item("ACAD");
            dynamic toolbar = menuGroup.Toolbars.Add(name);
            foreach (var item in items)
            {
                if (item is RibbonButton tmp)
                {

                    addbaritem(toolbar, tmp);
                }
            }
            return toolbar;
        }
        #endregion



        #endregion

    }

}

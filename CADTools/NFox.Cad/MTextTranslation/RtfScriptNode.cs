﻿
namespace NFox.Cad.MTextTranslation
{
    public enum RtfScriptNodeType
    {
        Fraction,
        Italic,
        Tolerance
    }

    public class RtfScriptNode : RtfNode
    {

        private static string[] _scriptTypStrings =
            new string[] { "/", "#", "^" };

        public string SuperScript { get; set; }
        public string SubScript { get; set; }

        public RtfScriptNode(RtfScriptNodeType nodeType)
        {
            _nodeClassType = RtfNodeClassType.Script;
            _key = (int)nodeType;
        }

        public RtfScriptNode(RtfScriptNodeType nodeType, string superScript, string subScript)
        {
            _nodeClassType = RtfNodeClassType.Script;
            _key = (int)nodeType;
            SuperScript = superScript;
            SubScript = subScript;
        }

        public override string Contents =>
            string.Format(
                "\\S{0}{1}{2};",
                SuperScript,
                _scriptTypStrings[_key],
                SubScript);

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Autodesk.Windows;


namespace CADTools
{

    public class Tab : RibbonTab, IEnumerable<RibbonPanel>
    {
        /// <summary>
        /// 初始化Ribbon选项卡
        /// </summary>
        /// <param name="title">选项卡的名字，默认生成$"ID_Tab_{title}"的ID</param>
        public Tab(string title)
        {
            Title = title;
            Id = $"ID_Tab_{title}"; 
        }

        public void Add(RibbonPanel item)
        {
            Panels.Add(item);
        }

        public IEnumerator<RibbonPanel> GetEnumerator()
        {
            return Panels.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Panels.GetEnumerator();
        }
    }
}

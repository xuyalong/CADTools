﻿using System;
using System.Collections.Generic;
using System.Linq;

using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

namespace NFox.Cad.ExtendMethods
{
    public static class SelectionSetEx
    {

        public static IEnumerable<SelectedObject> GetSelectedObjects(this SelectionSet ss)
        {
            return ss.Cast<SelectedObject>();
        }

        public static IEnumerable<T> GetSelectObjects<T>(this SelectionSet ss) where T : SelectedObject
        {
            return ss.Cast<SelectedObject>().OfType<T>();
        }

        public static IEnumerable<ObjectId> GetObjectIds<T>(this SelectionSet ss) where T : Entity
        {
            string dxfName = RXClass.GetClass(typeof(T)).DxfName;
            return
                ss
                .GetObjectIds()
                .Where(id => id.ObjectClass.DxfName == dxfName);
        }

        public static IEnumerable<IGrouping<string, ObjectId>> GetObjectIdDictionary(this SelectionSet ss)
        {
            return
                ss
                .GetObjectIds()
                .GroupBy(id => id.ObjectClass.DxfName);
        }

        #region GetEntitys

        public static IEnumerable<Entity> GetEntitys(this SelectionSet ss, Transaction tr, OpenMode openMode)
        {
            return
                ss
                .GetObjectIds()
                .Select(id => (Entity)tr.GetObject(id, openMode));
        }

        public static IEnumerable<Entity> GetEntitys(this SelectionSet ss, Transaction tr)
        {
            return GetEntitys(ss, tr, OpenMode.ForRead);
        }

        public static IEnumerable<T> GetEntitys<T>(this SelectionSet ss, Transaction tr, OpenMode openMode) where T : Entity
        {
            return 
                ss
                .GetObjectIds()
                .Select(id => tr.GetObject(id, openMode))
                .OfType<T>();
        }

        public static IEnumerable<T> GetEntitys<T>(this SelectionSet ss, Transaction tr) where T : Entity
        {
            return GetEntitys<T>(ss, tr, OpenMode.ForRead);
        }

        #endregion

        #region ForEach

        public static void ForEach<T>(this SelectionSet ss, Transaction tr, OpenMode openMode, Action<T> action) where T : Entity
        {
            foreach (T ent in ss.GetEntitys<T>(tr, openMode))
            {
                action(ent);
            }
        }

        public static void ForEach<T>(this SelectionSet ss, Transaction tr, Action<T> action) where T : Entity
        {
            foreach (T ent in ss.GetEntitys<T>(tr))
            {
                action(ent);
            }
        }

        public static void ForEach(this SelectionSet ss, Transaction tr, OpenMode openMode, Action<Entity> action)
        {
            foreach (Entity ent in ss.GetEntitys(tr, openMode))
            {
                action(ent);
            }
        }

        public static void ForEach(this SelectionSet ss, Transaction tr, Action<Entity> action)
        {
            foreach (Entity ent in ss.GetEntitys(tr))
            {
                action(ent);
            }
        }

        #endregion

    }
}

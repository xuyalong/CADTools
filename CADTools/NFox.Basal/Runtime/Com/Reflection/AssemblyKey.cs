﻿using System;
using System.Collections.Generic;
using System.IO;


namespace NFox.Runtime.Com.Reflection
{
    /// <summary>
    /// Com程序集关键字
    /// </summary>
    public struct AssemblyKey : IEqualityComparer<AssemblyKey>
    {

        /// <summary>
        /// 初始化例程
        /// </summary>
        /// <param name="guid">类型ID</param>
        /// <param name="major">主版本号</param>
        /// <param name="minor">次版本号</param>
        public AssemblyKey(string guid, short major, short minor)
        {
            ClsId = guid;
            Major = major;
            Minor = minor;
        }

        public string ClsId { get; }

        public string Version
        {
            get { return $"{Major}.{Minor}"; }
        }

        public short Major { get; }

        public short Minor { get; }


        public bool Equals(AssemblyKey x, AssemblyKey y)
        {
            return 
                (x.ClsId == y.ClsId) && 
                (x.Major == y.Major) &&
                (x.Minor == y.Minor);
        }

        public int GetHashCode(AssemblyKey obj)
        {
            return ClsId.GetHashCode() * 31 + Major.GetHashCode();
        }
    }
}

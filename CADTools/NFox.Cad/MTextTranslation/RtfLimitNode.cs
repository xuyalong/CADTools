﻿
namespace NFox.Cad.MTextTranslation
{

    public enum RtfLimitNodeType
    {

        Underline,        //L
        Strikeout,        //O

    }

    public class RtfLimitNode : RtfNode
    {

        private readonly static string[] _typeCodes =
            new string[] {"L", "O" };

        public RtfLimitNode(RtfLimitNodeType nodeType)
        {
            _nodeClassType = RtfNodeClassType.Limit;
            _key = (int)nodeType;
        }

        public override string Contents => $"\\{_typeCodes[_key]}";

    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NFox.Cad.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using NFox.Cad.ExtendMethods;


//[assembly: ExtensionApplication(typeof(CADTools.Init))]
namespace CADTools
{
    public class Init : AutoRegAssem //经测试只要这里继承IExtensionApplication 接口就可以实现启动加载了
    {

        private CompositionContainer _container;
        private DirectoryCatalog catalog;
        private RibbonControl control;
        private static List<RibbonTab> ribbonTabs = new List<RibbonTab>();
        private DirectoryInfo PluginPath
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return new DirectoryInfo(Path.Combine(Path.GetDirectoryName(path), "Plugins"));
            }
        }
        //[ImportMany(typeof(IPlugin))]
        //public IEnumerable<Lazy<IPlugin, IMetaData>> Plugins { get; private set; }

        [ImportMany(typeof(IPlugin))]
        public IEnumerable<IPlugin> Plugins { get; private set; }
        public void Load()
        {
            
            catalog = new DirectoryCatalog(PluginPath.FullName, "*.dll");
            AddComposeParts(catalog);
            ComponentManager.ItemInitialized += ComponentManager_ItemInitialized;
        }

        private void ComponentManager_ItemInitialized(object sender, RibbonItemEventArgs e)
        {
            if (ComponentManager.Ribbon != null)
            {
                control = ComponentManager.Ribbon;
                LoadMenuAndRibbon();
                LoadExtendDll(catalog);
                ComponentManager.ItemInitialized -= ComponentManager_ItemInitialized;
            }
        }

        private void LoadMenuAndRibbon()
        {
            Tab tab =
               new Tab("插件平台")
               {
                    new Panel("设置")
                    {
                        {"设置","line", Properties.Resources.setup}
                    }
               };
            //ribbonTabs.Add(tab);
            control.AddRibbon(tab);
            Menu.AddMenu(tab);
            //Menu.AddToolBar(tab);

        }

        private void AddComposeParts(ComposablePartCatalog catalog)
        {
            _container = new CompositionContainer(catalog);
            try
            {
                _container.ComposeParts(this);
            }
            catch (CompositionException compositionEx)
            {
                Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(compositionEx.ToString());
            }
        }

        private void LoadExtendDll(DirectoryCatalog path)
        {
            var doc = Application.DocumentManager.MdiActiveDocument;
            foreach (var dll in path.LoadedFiles)
            {
                try
                {
                    Assembly ab = Assembly.LoadFrom(dll.ToUpper());
                    Type[] types = ab.GetTypes();
                    foreach (var item in types)
                    {
                        if (item.GetInterface("IPlugin") != null)
                        {
                            var obj = ab.CreateInstance(item.FullName);
                            var field = obj.GetType().GetProperty("MenuTab");
                            var tab = (RibbonTab)field.GetValue(obj);
                            control.AddRibbon(tab);
                            Menu.AddMenu(tab);
                            //Menu.AddToolBar(tab);

                            //ribbonTabs.Add(tab);
                        }
                    }
                }
                catch (Autodesk.AutoCAD.Runtime.Exception e)
                {
                    doc.Editor.WriteMessage(e.Source);
                }

            }
            //control.AddRibbon(ribbonTabs.ToArray());
            //Menu.AddMenu(ribbonTabs.ToArray());

        }
        public override void Initialize()
        {
            Load();
        }

        public override void Terminate()
        {

        }
    }
}



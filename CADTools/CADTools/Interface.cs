﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Autodesk.Windows;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
namespace CADTools
{


    
    public interface IPlugin  
    {
        /// <summary>
        /// 为了定义插件的菜单及ribbon界面
        /// </summary>
        RibbonTab MenuTab { get; }
    }


    /* 目前下面的部分是没有用的，如果将来为了对插件的元数据进行更多的处理，可以进行自定义特性的相关操作。
    /// <summary>
    /// 元数据接口，为了MEF提供元数据的导出
    /// </summary>
    public interface IMetaData
    {
        string Name { get; } //插件命令名称
        string Macro { get; } //插件命令字符串
        [DefaultValue(0)]
        int SubMenuGroupId { get; } //插件在菜单栏中的子菜单的位置，默认为0，数字相同的，看命运的安排
        string SubMenuName { get; } //子菜单的名字字符串
        [DefaultValue(0)]
        int MenuItemId { get; } //菜单项的位置，默认为0，数字相同的，看命运的安排
        //RibbonTab Tab { get; }
    }

    /// <summary>
    /// 定义菜单的导出属性
    /// </summary>
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class MenuExportMetaDataAttribute : ExportAttribute, IMetaData
    {

        public string Name { get; private set; }
        public string Macro { get; private set; }
        public int SubMenuGroupId { get; private set; }
        public string SubMenuName { get; private set; }
        public int MenuItemId { get; private set; }


        public MenuExportMetaDataAttribute(string name = "", string macro = "", int subId = 0, string subName = "", int itemId = 0) : base(typeof(IMetaData))
        {
            Name = name;
            Macro = macro;
            SubMenuGroupId = subId;
            SubMenuName = subName;
            MenuItemId = itemId;

        }
        //public RibbonTab Tab { get; private set; }
        //public MenuExportMetaDataAttribute(RibbonTab tab) : base(typeof(IMetaData))
        //{
        //    Tab = tab;
        //}
    }

    */

}

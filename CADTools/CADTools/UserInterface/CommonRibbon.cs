﻿using Autodesk.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Resources;
using Autodesk.Private.Windows;

namespace CADTools
{
    public static class Ribbon
    {

        //面板的按钮是按行排列，即row，所以涉及到换行的时候，多行的时候，要用ribbonrowbreak换行
        //面板下面隐藏的按钮，需要面板换行，用ribbonpanelbreak，不然不会自动隐藏
        //面板里按列排列的按钮要放在ribbonrowpanel的items里,同时要在每个按钮的后面加上分隔符和换行,顺序不能乱

        #region 通用版本API
        /// <summary>
        /// 添加ribbon选项卡     
        /// </summary>
        /// <param name="control">ribbon控件对象</param>
        /// <param name="title">选项卡名字，默认id=$"ID_Tab_{title}"</param>
        /// <param name="isActive">是否激活</param>
        /// <returns>ribbon选项卡</returns>
        public static RibbonTab AddTab(this RibbonControl control, string title, bool isActive)
        {
            RibbonTab tab = new RibbonTab
            {
                Title = title,
                Id = $"ID_Tab_{title}"
            };
            control.Tabs.Add(tab);
            tab.IsActive = isActive;
            return tab;
        }
        /// <summary>
        /// 添加ribbon面板
        /// </summary>
        /// <param name="tab">ribbon选项卡</param>
        /// <param name="title">面板名字</param>
        /// <returns>ribbon面板资源对象</returns>
        public static RibbonPanelSource AddPanel(this RibbonTab tab, string title)
        {
            RibbonPanelSource source = new RibbonPanelSource
            {
                Title = title
            };
            RibbonPanel panel = new RibbonPanel
            {
                Source = source
            };
            tab.Panels.Add(panel);
            return source;
        }

        /// <summary>
        /// 添加ribbon按钮
        /// </summary>
        /// <param name="panel">ribbon面板资源对象</param>
        /// <param name="name">按钮名字</param>
        /// <param name="cmd">按钮要执行的命令</param>
        /// <param name="size">按钮尺寸大小</param>
        /// <param name="orient">按钮名字相对图标的位置</param>
        /// <returns>ribbon按钮</returns>
        public static RibbonButton AddButton(this RibbonPanelSource panel, string name, string cmd,
            RibbonItemSize size = RibbonItemSize.Standard, Orientation orient = Orientation.Horizontal)
        {
            RibbonButton button = new RibbonButton
            {
                Name = name,//按钮的名称
                Text = name,
                ShowText = true, //显示文字

                Size = size, //按钮尺寸
                Orientation = orient, //按钮排列方式
                CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
                CommandParameter = cmd + " ",
                ShowImage = true, //显示图片

            };

            panel.Items.Add(button);
            return button;
        }
        /// <summary>
        /// 添加ribbon按钮
        /// </summary>
        /// <param name="panel">ribbon下拉按钮对象</param>
        /// <param name="name">按钮名字</param>
        /// <param name="cmd">按钮要执行的命令</param>
        /// <param name="size">按钮尺寸大小</param>
        /// <param name="orient">按钮名字相对图标的位置</param>
        /// <returns>ribbon按钮</returns>
        public static RibbonButton AddButton(this RibbonSplitButton panel, string name, string cmd,
            RibbonItemSize size = RibbonItemSize.Standard, Orientation orient = Orientation.Horizontal)
        {
            RibbonButton button = new RibbonButton
            {
                Name = name,//按钮的名称
                Text = name,
                ShowText = true, //显示文字

                Size = size, //按钮尺寸
                Orientation = orient, //按钮排列方式
                CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
                CommandParameter = cmd + " ",
                ShowImage = true, //显示图片
            };


            panel.Items.Add(button);

            return button;
        }
        /// <summary>
        /// 添加ribbon按钮
        /// </summary>
        /// <param name="panel">ribbon行面板对象</param>
        /// <param name="name">按钮名字</param>
        /// <param name="cmd">按钮要执行的命令</param>
        /// <param name="size">按钮尺寸大小</param>
        /// <param name="orient">按钮名字相对图标的位置</param>
        /// <returns>ribbon按钮</returns>
        public static RibbonButton AddButton(this RibbonRowPanel panel, string name, string cmd,
            RibbonItemSize size = RibbonItemSize.Standard, Orientation orient = Orientation.Horizontal)
        {
            RibbonButton button = new RibbonButton
            {
                Name = name,//按钮的名称
                Text = name,
                ShowText = true, //显示文字

                Size = size, //按钮尺寸
                Orientation = orient, //按钮排列方式
                CommandHandler = new RibbonCommandHandler(),//给按钮关联命令
                CommandParameter = cmd + " ",
                ShowImage = true, //显示图片
            };
            panel.Items.Add(button);
            return button;
        }

        public static void AddButton(this RibbonPanelSource source, RibbonItemCollection items)
        {
            foreach (var item in items)
            {
                source.Items.Add(item);
            }
        }

        /// <summary>
        /// 添加分隔符
        /// </summary>
        /// <param name="source">ribbon面板资源对象</param>
        /// <param name="style">分隔符类型</param>
        public static void AddSep(this RibbonPanelSource source, RibbonSeparatorStyle style)
        {
            RibbonSeparator separator = new RibbonSeparator
            {
                SeparatorStyle = style
            };
            source.Items.Add(separator);
        }
        /// <summary>
        /// 添加分隔符
        /// </summary>
        /// <param name="source">ribbon行面板对象</param>
        /// <param name="style">分隔符类型</param>
        public static void AddSep(this RibbonRowPanel source, RibbonSeparatorStyle style)
        {
            RibbonSeparator separator = new RibbonSeparator
            {
                SeparatorStyle = style
            };
            source.Items.Add(separator);
        }
        /// <summary>
        /// 添加ribbon行面板
        /// </summary>
        /// <param name="source">ribbon面板资源对象</param>
        /// <returns>ribbon行面板</returns>
        public static RibbonRowPanel AddRowPanel(this RibbonPanelSource source)
        {
            RibbonRowPanel panel = new RibbonRowPanel();
            source.Items.Add(panel);
            return panel;

        }
        /// <summary>
        /// 添加ribbon换行符
        /// </summary>
        /// <param name="source">ribbon面板资源对象</param>
        public static void AddRowBreak(this RibbonPanelSource source)
        {
            RibbonRowBreak ribbonPanelBreak = new RibbonRowBreak();
            source.Items.Add(ribbonPanelBreak);
        }
        /// <summary>
        /// 添加ribbon换行符
        /// </summary>
        /// <param name="source">ribbon行面板对象</param>
        public static void AddRowBreak(this RibbonRowPanel source)
        {
            RibbonRowBreak ribbonPanelBreak = new RibbonRowBreak();
            source.Items.Add(ribbonPanelBreak);
        }
        /// <summary>
        /// 添加ribbon面板换行符，该对象之后的按钮全部默认隐藏
        /// </summary>
        /// <param name="source">ribbon面板资源对象</param>
        public static void AddPanelBreak(this RibbonPanelSource source)
        {
            RibbonPanelBreak ribbonPanelBreak = new RibbonPanelBreak();
            source.Items.Add(ribbonPanelBreak);
        }

        /// <summary>
        /// 添加ribbon下拉按钮
        /// </summary>
        /// <param name="source">ribbon面板资源对象</param>
        /// <param name="text">下拉按钮名字</param>
        /// <param name="size">下拉按钮的尺寸大小</param>
        /// <param name="orientation">按钮名字相对图标的位置</param>
        /// <returns>ribbon下拉按钮</returns>
        public static RibbonSplitButton AddSplitButton(this RibbonPanelSource source, string text, RibbonItemSize size = RibbonItemSize.Large, Orientation orientation = Orientation.Horizontal)
        {
            RibbonSplitButton splitButton = new RibbonSplitButton
            {
                Text = text,
                ShowText = true,
                Size = size,
                ShowImage = true,
                Orientation = orientation,
            };
            source.Items.Add(splitButton);
            return splitButton;
        }
        /// <summary>
        /// 添加ribbon下拉按钮
        /// </summary>
        /// <param name="source">ribbon行面板对象</param>
        /// <param name="text">下拉按钮名字</param>
        /// <param name="size">下拉按钮的尺寸大小</param>
        /// <param name="orientation">按钮名字相对图标的位置</param>
        /// <returns>ribbon下拉按钮</returns>
        public static RibbonSplitButton AddSplitButton(this RibbonRowPanel source, string text, RibbonItemSize size = RibbonItemSize.Large, Orientation orientation = Orientation.Horizontal)
        {
            RibbonSplitButton splitButton = new RibbonSplitButton
            {
                Text = text,
                ShowText = true,
                Size = size,
                ShowImage = true,
                Orientation = orientation,
            };
            source.Items.Add(splitButton);
            return splitButton;
        }
        /// <summary>
        /// 设置按钮的图标
        /// </summary>
        /// <param name="button">ribbon按钮</param>
        /// <param name="imgFileName">图标路径</param>
        /// <returns>按钮对象</returns>
        public static RibbonButton SetImage(this RibbonButton button, string imgFileName)
        {
            try
            {
                Uri uri = new Uri(imgFileName);
                BitmapImage bitmapImge = new BitmapImage(uri);
                button.Image = bitmapImge; //按钮图片
                button.LargeImage = bitmapImge; //按钮大图片
            }
            catch (Exception)
            {
            }

            return button;
        }
        /// <summary>
        /// 添加按钮提示
        /// </summary>
        /// <param name="button">ribbon按钮</param>
        /// <param name="title">提示名字</param>
        /// <param name="content">提示内容</param>
        /// <param name="cmd">提示的命令</param>
        /// <param name="expandContent">提示的扩展内容</param>
        /// <param name="imgFileName">提示的图标路径</param>
        /// <returns>ribbon按钮</returns>
        public static RibbonButton SetToolTip(this RibbonButton button, string title = "", string content = "", string cmd = "", string expandContent = "", string imgFileName = "")
        {
            RibbonToolTip toolTip = new RibbonToolTip();
            toolTip.Title = title;
            toolTip.Content = content;
            toolTip.Command = cmd;
            toolTip.ExpandedContent = expandContent;
            try
            {
                Uri toolTipUri = new Uri(imgFileName);
                BitmapImage toolTipBitmapImge = new BitmapImage(toolTipUri);
                toolTip.ExpandedImage = toolTipBitmapImge;
            }
            catch (Exception)
            {
            }

            button.ToolTip = toolTip;

            return button;

        }

        #endregion

        #region 快捷版本API，用于cadtools

        public static void AddRibbon(this RibbonControl control, RibbonTab tab)
        {
            var tabItem = control.FindTab(tab.Id); //获取同名tab
            if (tabItem != null) //找到同名tab
            {
                var panels = tabItem.GetPanels(); //获取同名tab的panelsource的名字
                foreach (var item in tab.Panels)
                {
                    //TODO: 是否采用id的方式处理更科学
                    if (panels.Contains(item.Source.Title)) //如果有同名panel
                    {
                        tabItem.Panels[panels.IndexOf(item.Source.Title)].Source.AddButton(item.Source.Items);
                    }
                    else //没有同名panel
                    {
                        tabItem.Panels.Add(item);
                    }
                }

            }
            else //未找到同名tab
            {
                control.Tabs.Add(tab);
            }

        }

        /// <summary>
        /// 获取选项卡里所有的面板的名字，实际获取的是面板资源的名字
        /// </summary>
        /// <param name="tab"></param>
        /// <returns></returns>
        public static List<string> GetPanels(this RibbonTab tab)
        {
            List<string> list = new List<string>();
            foreach (var item in tab.Panels)
            {
                list.Add(item.Source.Title);
            }
            return list;
        }

        /// <summary>
        /// 定义添加ribbon界面的函数
        /// </summary>
        /// <returns></returns>
        public static void AddRibbon(this RibbonControl control, params RibbonTab[] tab)
        {

            foreach (var item in tab)
            {
                control.AddRibbon(item);
            }
        }
        #endregion







    }
}

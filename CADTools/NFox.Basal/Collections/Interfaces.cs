﻿using System;
using System.Xml.Serialization;

namespace NFox.Collections
{
    public interface ISerializableCollection : IXmlSerializable
    {
        void WriteXml(string path);
        void ReadXml(string path);

        void WriteTo(string path);
        ISerializableCollection ReadFrom(string path);
    }

    public interface IItems<T>
    {
        Action<T> ItemAdded { get; set; }
        Action<T> ItemRemoving { get; set; }
        Action<T> ItemChanged { get; set; }
    }

}

﻿using System.Linq;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

namespace NFox.Cad.ExtendMethods
{
    public static class ObjectIdEx
    {

        #region GetObject

        public static DBObject GetObject(this ObjectId id, Transaction tr, OpenMode mode, bool openErased)
        {
            return tr.GetObject(id, mode, openErased);
        }

        public static DBObject GetObject(this ObjectId id, Transaction tr, OpenMode mode)
        {
            return id.GetObject(tr, mode, false);
        }

        public static DBObject GetObject(this ObjectId id, Transaction tr) 
        {
            return id.GetObject(tr, OpenMode.ForRead, false);
        }

        public static T GetObject<T>(this ObjectId id, Transaction tr, OpenMode mode, bool openErased) where T : DBObject
        {
            return (T)tr.GetObject(id, mode, openErased);
        }

        public static T GetObject<T>(this ObjectId id, Transaction tr, OpenMode mode) where T : DBObject
        {
            return id.GetObject<T>(tr, mode, false);
        }

        public static T GetObject<T>(this ObjectId id, Transaction tr) where T : DBObject
        {
            return id.GetObject<T>(tr, OpenMode.ForRead, false);
        }

        public static IEnumerable<T> GetObject<T>(this IEnumerable<ObjectId> ids, Transaction tr, OpenMode mode, bool openErased) where T : DBObject
        {
            return ids.Select(id => id.GetObject<T>(tr, mode, openErased));
        }

        public static IEnumerable<T> GetObject<T>(this IEnumerable<ObjectId> ids, Transaction tr, OpenMode mode) where T : DBObject
        {
            return ids.Select(id => id.GetObject<T>(tr, mode));
        }

        public static IEnumerable<T> GetObject<T>(this IEnumerable<ObjectId> ids, Transaction tr) where T : DBObject
        {
            return ids.Select(id => id.GetObject<T>(tr));
        }

        public static IEnumerable<DBObject> GetObject(this IEnumerable<ObjectId> ids, Transaction tr, OpenMode mode, bool openErased)
        {
            return ids.Select(id => id.GetObject(tr, mode, openErased));
        }

        public static IEnumerable<DBObject> GetObject(this IEnumerable<ObjectId> ids, Transaction tr, OpenMode mode)
        {
            return ids.Select(id => id.GetObject(tr, mode));
        }

        public static IEnumerable<DBObject> GetObject(this IEnumerable<ObjectId> ids, Transaction tr)
        {
            return ids.Select(id => id.GetObject(tr));
        }

        #endregion

        #region Open

        //public static T Open<T>(this ObjectId id, OpenMode mode, bool openErased) where T : DBObject
        //{
        //    return (T)id.Open(mode, openErased);
        //}

        //public static T Open<T>(this ObjectId id, OpenMode mode) where T : DBObject
        //{
        //    return (T)id.Open(mode, false);
        //}

        //public static T Open<T>(this ObjectId id) where T : DBObject
        //{
        //    return (T)id.Open(OpenMode.ForRead, false);
        //}

        #endregion

        public static IEnumerable<ObjectId> OfType<T>(this IEnumerable<ObjectId> ids) where T : DBObject
        {
            string dxfName = RXClass.GetClass(typeof(T)).DxfName;
            return
                ids
                .Where(id => id.ObjectClass.DxfName == dxfName);
        }

    }
}
